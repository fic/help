---
date: 2022-12-10T13:42:27+01:00
title: Déroulement
weight: 20
---

## Choisir son équipe (1 mai 2024 à 23h42) {#0-team}

- **Chaque équipe doit se composer de 6 membres, parmi les SRS.**

  Choisissez les membres de vos groupes en fonction de vos affinités vis-à-vis
  de la complémentarité des technologies que vous souhaitez dans vos scénarios.

  Vous pouvez regrouper 2 groupes de DM (si vous avez confiance dans les
  membres de vos deux groupes) ou partir sur de nouvelles têtes (si vous ne
  connaissez pas bien les membres de vos groupes de DM et que vous souhaitez
  répartir les risques).


- **Déterminez plusieurs thématiques pour vos scénarios.**

  Dans chaque scénario, vous allez faire évoluer une entreprise à travers un
  certain nombre de péripéties que vous devrez choisir vous-même.

  En groupe, déterminez pour chaque scénario :

    - le domaine que vous ciblez : choisissez un domaine avec lequel vous êtes
      familiers ou pour lequel vous pouvez trouver des contacts qui pourront
      vous renseigner. Par exemple si vos parents travaillent dans le milieu
      médical, vous savez certainement comment fonctionne un hôpital dans sa
      hiérarchie, ou peut-être que vous savez qu'il est facile dans un cabinet
      médical de brancher une clef USB sur l'ordinateur de la secrétaire à
      l'accueil.

      Si vous avez des connaissances dans le milieu industriel, allez les
      interroger pour voir à quoi ressemble leur système et concevez un système
      crédible à partir de ce que vous avez pu apprendre (il ne s'agit pas de
      le recopier).

      Si vous souhaitez orienter votre carrière dans un domaine particulier
      après l'école, n'hésitez pas à rencontrer des gens de ce milieu. Par
      exemple en 2020, nous avons pu avoir un scénario intégrant un SI de la
      marine, avec l'aide de [la chaire de
      cyberdéfense](https://www.chaire-cyber.fr/), car un 2020 voulait intégrer
      la marine après son cursus.

      Au-delà de l'enseignement, commencer à être en contact avec un milieu que
      vous visez peut être particulièrement motivant, profitez-en !


    - les technologies que vous aimeriez mettre en œuvre : souhaitez-vous plus
      réaliser des attaques réseaux, du hardware, des attaques sur les systèmes
      Linux, Windows, Android, iOS, de l'embarqué, des systèmes
      industriels, des trains, des drônes, les réseaux de communication, du
      social engineering, reverse engineering, des mainframes, ...

      On peut imaginer de nombreux scénarios autour du cloud. Pour cela il
      faudra vous rapprocher de personnes compétentes pour challenger vos
      infrastructures et peut-être vous mettre sur la piste d'erreurs courantes
      de sécurité, que vous pourriez introduire dans vos scénarios.

L'idée à la fin de ce brainstorming est que chacun soit hyper enthousiaste de
travailler toute l'année avec ces technologies. Sinon, il est encore temps de
changer de groupe.


## Phase 1 : petits exercices de forensic (avril à juin 2024) {#1-exercice-forensic}

Afin de vous familiariser avec le forensic, nous vous proposerons dans un premier temps de réaliser une série de quatre exercices de forensic, sur les thématiques qui vous intéressent. À raison d'un exercice par mois.

Pour commencer, nous vous donnerons des idées d'exercices tout prêts, pour lesquels ils ne vous restera plus qu'à mettre en place l'infrastructure de l'exercice et à enregistrer les traces proposées. L'idée est de vous familiariser avec les attentes du projet.

Lors du premier suivi (fin avril), nous évaluerons votre exercice et vous ferons des retours à prendre en compte pour l'améliorer si besoin. Vous devrez également présenter succinctement l'exercice sur lequel vous allez travailler ensuite. Vous pourrez prendre pour inspiration les idées d'exercices auxquelles on a réfléchi, cela vous assure d'avoir une trame cohérente, mais il faudra que vous réfléchissiez à l'infrastructure à mettre en place, aux traces qu'il faudra enregistrer, ...

Pour les deniers exercices, ce sera à vous d'établir sa thématique, son contexte, l'infrastructure et de faire l'enregistrement des traces.


## Phase 1 bis : élaboration du scénario (mai à juillet 2024) {#1-preparation-scenario}

En parallèle de la réalisation des exercices de forensic, vous allez devoir réfléchir au scénario d'attaque que vous allez réaliser au second semestre.

Il vous est demandé durant cette phase de recherche de regrouper vos idées : quelles cibles, quels acteurs, quelles techniques, ...

Lorsque vous aurez une vision un peu plus claire de votre scénario, que les assistants auront validé avec vous l'intérêt, la faisabilité et l'originalité de votre proposition, nous rechercherons un ancien compétent dans le(s) domaine(s) que vous ciblez, afin que vous puissiez échanger avec lui sur ce qui se fait dans le milieu.

Il est important que l'infrastructure que vous allez mettre en place reflète ce qui se fait dans le milieu que vous ciblez. Cet ancien pourra vous renseigner sur les outils de sécurité ou les émulateurs de matériel spécifiques disponibles dans le milieu.

De plus, n'oubliez pas que c'est une occasion pour rencontrer un intervenant d'un milieu qui vous intéresse : vous pourrez ainsi échanger avec lui et mettre un pied dans l'environnement technique. C'est un détail qui peut faire la différence lors de votre recherche de stage, d'avoir cette connaissance préalable du milieu.


## Phase 2 : réalisation du scénario (juillet à décembre 2024) {#2-conception-scenario}

Une fois votre scénario bien ficelé, vous pourrez vous atteler à sa réalisation.

Il faudra bien sûr commencer par mettre en place toute l'infratructure, réfléchir à l'enchaînement des étapes pour que la progression soit fluide pour les participants (par exemple si vous fixez la date de l'attaque au 25 décembre 2023, il s'agit de bien respecter cette date dans toutes les traces que vous allez donner).

Entre septembre et novembre, vous allez devoir réaliser les 5 paliers de ce scénario. Une soutenance viendra valider systématiquement vos réalisations.


## Phase 3 : réalisation d'un petit exercice de forensic ardu (décembre 2024) {#3-exercice-forensic}

Forts de votre année de SRS, nous vous demandons de réaliser un dernier exercice de forensic, a priori indépendant de votre scénario. Cet exercice sera l'apothéose de vos compétences. Le scénario pouvant parfois vous contraindre dans certaines directions, avec cet exercice vous pourrez laisser libre court à votre imagination pour concevoir un exercice indépendant, bien pimenté, avec des choix de techniques que vous ne soupçoniez sans doute pas en arrivant en SRS !

Cela peut être une bonne idée de mutualiser vos efforts avec le cours de virologie : vous n'aurez ainsi que le contexte à rédiger et les traces à enregistrer.


## Phase 4 : test de la plate-forme et corrections (mai 2024 à janvier 2025) {#4-tests}

La plate-forme de soumission vit au rythme des améliorations dont vous aurez
envie.

Régulièrement, nous organisons des « FIC Blanc » pour vous permettre d'une part
de tester la plate-forme, dans le but de s'assurer que tout fonctionne bien, et
d'autre part, cela vous permet de tester entre-vous les exercices et scénarios
de vos camarades. En faisant des retours lorsque vous ne comprenez pas ou
lorsque vous rencontrez un bug, c'est grâce à ces sessions que l'on arrive à
proposer une expérience de jeu inégalée lors de la vraie compétition.


## Fin : l'EC2 durant le FIC 2025 (début avril 2025) {#4-ec2-fic}

Tous les contenus que vous allez préparer seront présentés durant la
compétition [*European Cyber Cup*](https://european-cybercup.com/).

Des équipes de professionnels de la cybersécurité et des équipes d'étudiants
habitués des CTF s'y affrontent durant le [Forum International de la
Cybersécurité](https://www.forum-fic.com/) qui a lieu tous les ans à Lille.

Vos exercices et scénarios seront arpentés par les 200 compétiteurs attendus.
