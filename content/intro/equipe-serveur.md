---
date: 2022-12-10T15:29:26+01:00
title: L'équipe serveur
weight: 25
---

L'équipe serveur se compose généralement de 3 à 4 étudiants volontaires.

## Quels sont leurs rôles ?

- Préparer l'infrastructure des serveurs que l'on apporte au FIC ;
- aider les participants durant le challenge et gérer les problèmes techniques ;
- aider les SRS durant la préparation du challenge en répondant aux problèmes
  courants ;
- maintenir le site [`fic.srs.epita.fr`](https://fic.srs.epita.fr/) en état et
  le mettre à jour ;
- maintenir et contribuer au projet
  [`fic-server`](https://gitlab.cri.epita.fr/ing/majeures/srs/fic/server/) ;
- s'entraîner sur l'infrastructure durant les [FIC Blancs]({{% relref "deroulement.md" %}}).



<!--
## Qui sont-ils ?

-->

## Comment postuler ?

Faites-vous connaître sur la liste de diffusion
[`srs-fic-server@ml.cri.epita.fr`](mailto:srs-fic-server@ml.cri.epita.fr).

Tout le monde est le bienvenue dans l'équipe, quelles que soient vos appétences. À
partir du moment où l'une des activités listées ci-dessus vous intéresse, venez,
on vous accueillera avec grand plaisir.
