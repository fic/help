---
date: 2019-04-04T15:59:52+02:00
title: Présentation
weight: 5
---

Voici votre projet de fin d'études : **concevoir un challenge de forensic de A à Z**.

Vous trouverez dans cette section tous les points importants à savoir sur le projet.

Vous pouvez aussi télécharger la [présentation](presentation.pdf) pour les revoir.

Voici les différentes pages qui vous permettront d'en apprendre davantage sur le projet :

{{% children %}}
