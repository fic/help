---
date: 2022-12-10T13:42:27+01:00
title: Le projet FIC
weight: 15
---

Le projet `FIC_D` est votre projet de fin d'études. Il se veut être le couronnement de votre année de SRS. Nous attendons donc qu'il reflète vos capacités techniques et démontre votre capacité à produire du contenu de qualité.

Contrairement aux autres projets, ici vous avez une obligation de résultats : vous n'avez pas la possibilité d'abandonner ou de juste viser la moyenne. Votre production doit être présentable à un public et nous, encadrants, sommes avec vous pour nous assurer de la qualité finale, vous orienter et vous aider tout au long de l'année vers cet objectif.


## Les délais

Votre projet de fin d'études vous accompagne sur presque toute votre année de SRS. Nous vous le présentons vers fin mars, lorsque vous avez pu commencer à prendre vos marques dans la majeure et pu rencontrer vos camarades. Il dure jusqu'à votre sortie de SRS, en janvier.

Cela fait donc près de 9 mois (avec une pause au mois d'août) que vous allez consacrer à travailler régulièrement sur ce sujet.


## Le suivi

Contrairement à d'autres projets de fin d'études où le suivi peut être aléatoire et dépendre de facteurs extérieurs, ici vous savez que chaque mois, vous aurez 1 samedi matin dédié au suivi de votre avancement, au minimum.

D'autres suivis peuvent vous être proposés par l'équipe d'encadrants, si vous rencontrez des difficultés particulières ou que l'on juge que vous prennez du retard sur le planning.

Enfin, des *FIC Blancs* sont organisés aussi régulièrement que possible pour vous permettre de récolter des retours sur la faisabilité de vos exercices.


### Suivi mensuel

1 samedi par mois (selon vos disponibilités d'emploi du temps, cela peut varier légèrement), une matinée est consacréée au suivi et à l'avancement du projet.

À cette occasion, chaque groupe de 6 passera devant un jury aléatoire de 2 encadrants qui seront là uniquement pour juger votre dernière réalisation et valider le lancement de la suivante.

Il est attendu que vous présentiez à l'aide d'un vidéoprojecteur, rapidement et succinctement, le contexte de votre exercice sur la plateforme, puis sa résolution (en montrant [le write-up]({{% relref "../files/write-up.md" %}}) ou la [vidéo de résolution]({{% relref "../files/resolution.md" %}})).

Les encadrants testerons sur la plateforme les flags, vérifierons la cohérence du contexte et sa présentabilité (fautes d'ortographes, qualité, ...)


## La notation

Vous aurez deux notes : une à la fin de chaque semestre.

Comme dit en introduction, la notation n'est pas réalisée sur votre capacité à atteindre l'objectif car vous avez une obligation de résultat : si vos encadrants jugent que vos exercices ne respectent pas les critères de qualité minimum pour être présentés, vous ne pourrez pas être évalués et n'aurez pas de note pour le contenu visé.

La notation porte sur la qualité de votre travail, sur le dépassement technique et sur ta tenue des délais.


### Qualité du travail produit

La qualité comprend la cohérence globale de votre projet, notamment au sein du scénario.

Il s'agit de relire votre travail, de constater qu'il n'y a ni faute d'ortographe ou de grammaire, ni non-sens technique, **au moment où vous présentez votre travail en soutenance**.

Votre travail doit être présentable publiquement, il représente l'école, la majeure.


### Dépassement technique

Nous évaluons ici l'originalité de vos propositions, votre capacité à imaginer et interpréter les défis qui vous sont proposés.

Mais aussi il s'agit d'évaluer la manière dont vous faites face aux difficultés. En effet, nous sommes là pour vous donner un cadre et vous aiguiller, mais pas pour vous aider techniquement dans la réalisation de vos exercices : ce projet est une synthèse de vos autres cours et de votre capacité de recherche.


### Tenue des délais

9 mois c'est long ... et court. Vous avez une année chargée, on le sait. Travailler régulièrement est votre meilleur atout pour mener à bien ce projet.

Votre capacité à planifier des séances de travail, à respecter les deadlines, anticiper les moments de rush, ... tout cela fait partie du projet FIC.

Lorsque d'autres priorités plus grandes, qui n'étaient pas anticipées, vous empêchent de terminer une étape, soyez honêtes et décrivez comment vous allez vous organiser pour rattraper le retard. Jusqu'à 24 heures avant la soutenance, vous pouvez nous demander exceptionnellement de reporter de quelques jours votre passage si cela vous permet de tenir les délais et les points précédents.

Nous apprécions aussi que vous prépareriez les soutenances (il n'est pas question de vous demander de passer du temps à répéter votre intervention, mais ayez un plan, que chacun sache quand il intervient et pour dire quoi). En arrivant dans la salle, installez-vous rapidement, avec du matériel testé apte à la projection (on ne recompile pas `xrandr` devant nous svp ...) et disposant de tous les exercices attendus à présenter (pour ne pas perdre de temps à changer d'ordi entre chaque exercice).
