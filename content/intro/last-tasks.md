---
date: 2023-01-15T13:42:27+01:00
title: Dernières tâches
weight: 25
draft: true
---

L'année touche bientôt à sa fin, et votre projet de fin d'étude aussi ! Nous sommes sur la dernière ligne droite, et avant le FIC Blanc de mardi prochain, nous avons encore 3 tâches à vous demander :

- [Clore les dernières issues](#issues),
- [traduire 1 scénario sur les 2, en anglais](#translation),
- [répondre aux requêtes des roots FIC concernant des imprécisions dans vos resolutions](#root-requests).


## Clore les dernières issues {#issues}

Certains groupes ont encore quelques issues ouvertes (que ce soit par moi-même, vous-même ou par des assistants). Nous vous demandons d'y répondre et de les résoudres d'ici le prochain FIC blanc.


## Avoir 1 scénario jouable en anglais {#translation}

Les organisateurs du FIC ont ouvert cette édition du challenge à des compétiteurs étrangers (il y avait quelques participants étrangers l'an dernier, cette année ce sera des équipes entières qui devraient ne pas forcément parler français).

Dans le but de permettre à ces équipes d'avoir toutes leurs chances, on nous a demandé de rendre tout notre challenge international. Comme c'est en contradiction avec les consignes qui primaient jusque là et que l'on sait que certains scénarios ne pourront pas être traduits sans refaire une partie des étapes, nous avons négocié que pour cette année, seul la moitié des nos défis seront traduits.

Chaque équipe SRS est donc tenue de choisir un scénario, qu'elle traduira entièrement en anglais. Il s'agit de choisir le scénario pour lequel le moins d'effort est nécessaire pour qu'il soit jouable sans avoir besoin de parler français. On évitera par exemple de choisir un scénario dans lequel une étape consiste à analyser une centaine de mails écrits en français ou des échanges de SMS, ...[^DEUXCOMMECA]

[^DEUXCOMMECA]: Dans le cas où vos deux scénarios contiendraient de trop nombreux contenus à traduire (mails, messages, ...), vous pouvez chercher une équipe dont le scénario qu'elle ne traduit pas serait également propice à la traduction, et le traduire en accord avec l'équipe, à la place de vos scénarios ; de telle sorte que l'on ait bien à la fin 50 % de contenu traduit.

Tout le contenu doit être traduit en place : `overview.md`, `statement.md`, `challenge.toml`, ... ainsi que les *hints* et *files* lorsque du français est utilisé sur des éléments dont la compréhension est indispensable pour résoudre l'étape. Gardez votre travail en français dans une branche `fr`. La version traduite ira sur la branche principale.

À l'exception des `resolution.md` et `resolution.mp4` où l'on vous demande **d'avoir les deux langues**.
Pour la vidéo, ajoutez une seconde piste de sous-titres, et pensez à définir l'information de régionalité de chaque piste, afin que l'on puisse choisir en connaissance de cause la bonne piste de sous-titres selon la langue affichée dans le lecteur.
Pour le write-up, placez la traduction dans un fichier `resolution.<locale>.md`, par exemple `resolution.en.md`, en gardant bien `resolution.md` en français.

Pour chaque scénario traduit, ajoutez un fichier `language.txt` à la racine du dépôt, contenant la locale concernée :

Par exemple, pour l'anglais :

```
en
```

Cela désactivera notamment grammalecte sur la plupart du dépôt (puisqu'il ne fonctionne que sur le français).


## Répondre aux requêtes des roots FIC {#root-requests}

Lors de la compétition en avril prochain, vos roots FIC vont notamment être sollicités par les participants qui auront une démarche juste, mais qui ne leur permet pas de valider les flags pour diverses raisons : soit parce qu'ils ne comprennent pas ce qui est demandé, soit parce qu'ils ne trouvent pas la bonne information demandée.

D'ici le FIC blanc, ils vont passer sur vos dépôts pour vérifier qu'ils sont bien en mesure, à partir de vos `resolution.md` et `resolution.mp4`, de résoudre l'intégralité de chaque étape. Que vous n'avez pas omis d'explication qui pourrait être gênante durant la compétition, si personne ne peut comprendre votre démarche.

Prenez ces requêtes très au sérieux car chaque année, lors de la compétition, nous sommes obligés de retirer des flags, voire des étapes complètes, car sur place nous n'arrivons pas à nous retrouver dans la même situation que celle présentée dans la vidéo.
