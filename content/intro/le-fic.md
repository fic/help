---
date: 2022-12-10T13:42:27+01:00
title: Le FIC et l'EC2
weight: 10
---

Le [Forum International de la Cybersécurité](https://www.forum-fic.com/) est un salon des professionnels du monde de la cybersécurité. Les acteurs du milieu s'y retrouvent pour avoir un aperçu des dernières innovations, se mettre au courant des dernières menaces et échanger avec leurs pairs.

Au sein de cet événement, la majeure SRS organise depuis 2014 un challenge de forensic qui s'est bien développé grâce aux talents de tous nos anciens, et bientôt des vôtres !

Depuis le FIC 2021, l'[European Cyber Cup (EC2)](https://european-cybercup.com/) est une véritable compétition. Elle a lieu durant le FIC sur deux jours, dans laquelle notre challenge s'inscrit, aux côtés d'autres challenges réalisés par des grands noms de la Cyber ([Root-Me](https://www.root-me.org/) pour le CTF par exemple, ...).

Ces dernières années, les participants sont 10 par équipes et s'affrontent sur 4 à 7 challenges en parallèle.

Les équipes sont soit des professionels de la cybersécurité (Quarkslab, Bouygues Telecom, Airbus, ...), soit des étudiants (UTT, EPITECH, EFREI, ...), pour un total de 250 participants en 2023.

Notre participation à cet événement, outre la démonstration de votre capacité à produire un contenu de très bonne qualité, est une opportunité pour vous de présenter votre travail auprès de vos futurs pairs.
