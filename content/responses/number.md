---
date: 2021-11-12T16:42:39+01:00
title: Flag nombre
weight: 6
---

Ce type de flag s'utilise pour valider une réponse attendue sous forme d'entier ou de floatant.

{{% notice warning %}}Attention, ce type de flag peut être relativement simple à bruteforcer. Ne l'utilisez que si les participants ont une large plage de valeurs parmi laquelle choisir.
{{% /notice %}}


### Exemple

```toml
[[flag]]
type = "number"
label = "Réponse à la question sur la vie, l'univers et le reste"
raw = 42
min = 12
max = 123456789
```

## Propriétés

Les mêmes propriétés que pour les [flags simples]({{% relref
"/responses/simple.md#propriétés" %}}) sont disponibles, et en plus :

max
: (facultatif) valeur maximale attendue dans le champ ;

min
: (facultatif) valeur minimale attendue dans le champ ;

step
: (facultatif, par défaut 1) interval entre deux valeurs autorisées.

## Rendu

![Rendu flag nombre](flag.png)

![Rendu flag nombre avec une unité](unit.png)
