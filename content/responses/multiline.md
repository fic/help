---
date: 2019-04-05T15:59:52+02:00
title: Flag multiligne
weight: 10
---

Ce type de flag s'utilise pour valider une chaîne de caractères que l'on
retrouve brute, qui fait plus d'une ligne. C'est le cas notamment des fichiers
de configuration, des clefs privées, des certificats, ...

### Exemple

```toml
[[flag]]
type = "text"
label = "Clef privée du service"
raw = """-----BEGIN RSA PRIVATE KEY-----
MIIEpQIBAAKCAQEA3Tz2mr7SZiAMfQyuvBjM9OiZ1BjP5CE/Wm/Rr500P
RK+Lh9x5eJPo5CAZ3/ANBE0sTK0ZsDGMak2m1g73VHqIxFTz0Ta1d+NAj
...
engiVoWc/hkj8SBHZz1n1xLN7KDf8ySU06MDggBhJ+gXJKy+gf3mF5Kmj
DtkpjGHQzPF6vOe907y5NQLvVFGXUq/FIJZxB8kfJdHEm2M4=
-----END RSA PRIVATE KEY-----
"""
```

Les mêmes attributs que pour les [flags simples]({{% relref
"/responses/simple.md#propriétés" %}}) sont disponibles.

## Rendu

![Rendu flag multiligne](flag-multiline.png)
