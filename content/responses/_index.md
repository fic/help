---
date: 2019-04-04T15:59:52+02:00
title: Flags
weight: 15
---

Les *flags* sont les éléments que les participants doivent retrouver pour passer à l'étape suivante.

Choisissez-les en faisant en sorte qu'il ne puisse pas y avoir d'ambiguïté, tout en empêchant l'anti-jeu, tel que le brute-force, ...

De nombreux types de flag sont à votre disposition :

{{% children %}}
