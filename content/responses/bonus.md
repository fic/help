---
date: 2022-10-08T16:06:30+02:00
title: Flag bonus
weight: 80
---

Certains flags peuvent ne pas être bloquant pour valider l'étape, mais néanmoins constituer une information intéressante à noter.
Cela peut être une information confidentielle que l'on retrouve dans les documents ou les conversations à analyser, ou un clin d'œil par rapport à des ressources qui vous ont servi à réaliser l'étape.

### Exemple

```toml
[[flag]]
label = "Date de sortie de Demie Vie 3"
placeholder = "dd/mm/yyyy"
raw = "12/12/2012"
bonus_gain = 5
```

## Propriétés

bonus_gain
: (facultatif) si présent, la validation de ce flag ne sera pas un prérequis pour valider l'étape. Lorsque ce flag est validé, indépendamment du reste de l'étape, il rapporte le nombre de points indiqué par cette propriété (potentiellement négatif).


## Rendu

![Rendu de flag optionnel](flag.png)
