---
date: 2022-01-21T14:39:52+01:00
title: Texte informatif
weight: 99
---

Il ne s'agit pas d'un flag à proprement parler car il ne donne pas lieu à une validation. Ceci permet de donner une information au participant, au sein du cadre affichant les flags.

### Exemple

```toml
[[flag]]
type = "label"
label = "Vous avez accès aux vidéos qui étaient stockées sur la machine."
variant = "danger"
```

## Propriétés

Ce type de flag reconnaît seulement les propriétés suivantes :

id
: (facultatif) identifiant du flag au sein de l'étape, pour définir des dépendances ;

label
: le texte à afficher aux participants ;

variant
: (facultatif) variante de couleur à utiliser, voir [les variantes définies par Bootstrap](https://getbootstrap.com/docs/5.0/utilities/colors/#colors). Il est attendu uniquement le nom de la variante : `primary` ou `warning`, et non pas `text-primary` ou `text-warning`.

Il est également possible de définir des dépendances, comme pour les autres types de flags.


## Rendu

![Rendu flag label](flag.png)
