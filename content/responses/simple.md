---
date: 2019-04-05T15:59:52+02:00
title: Flag simple
weight: 5
---

Ce type de flag s'utilise pour valider toute chaîne de caractères que l'on
retrouve brute dans les fichiers donnés (ou après un traitement
_déterministe_).

Attention, par défaut, les propositions ne sont pas sensibles à la
casse[^gocase].

[^gocase]: Casse selon les classes Unicode de caractères, cela ne concerne pas
    que l'alphabet traditionnel. Pour plus d'infos, consultez la [documentation
    de la fonction utilisée](https://golang.org/pkg/bytes/#ToLower).

### Exemple

```toml
[[flag]]
label = "IPv6 d'exfiltration"
raw = 'fe80::319c:1002:7c60:68fa'
```

Dans cet exemple, l'importance de la casse de l'IPv6 n'est pas importante, on
laissera donc le participant entrer l'IPv6 comme il le désire, même si elle
peut être copiée/collée.


## Flag strict, sensible à la casse

On utilise la propriété `casesensitive` pour forcer la prise en compte de la
casse lors de la vérification du flag.


### Exemple

```toml
[[flag]]
label = "Mot de passe du compte"
raw = 'rech1aichoh2Tei1ohHe'
casesensitive = true
```

Pour un mot de passe, ou une chaîne de caractères comme un token, ... qui sera
forcément recopiée par les participants, on impose le respect strict de la
casse.


## Flag modulable

Parfois, plusieurs réponses peuvent être valides. Le système de validation se
contentant d'une seule comparaison de hash, il ne peut valider qu'une seule et
unique solution.

Pour que plusieurs réponses soient valides, il faut pouvoir sélectionner des
parties communes significatives. Ces parties seront utilisées pour la création
du hash initial, et ensuite, lorsqu'il faudra vérifier les soumissions des
participants.

{{% notice warning %}}
Dans aucune circonstance il n'est possible de valider deux chaînes complètement
différentes. Par exemple, il n'est pas possible de valider :\
Donnez un ingrédient de la choucroute : `(choux|pomme de terre|...)`\
Car il n'est pas possible d'avoir un hash qui valide à la fois `choux` et
`pomme de terre`.
{{% /notice %}}

On utilise l'attribut `capture_regexp` pour établir une expression rationnelle
qui servira à sélectionner les parties significatives de la réponse.
Par exemple :

```toml
[[flag]]
label = "Heure de l'exfiltration"
raw = '11:22:33+02:00'
capture_regexp = "([0-9]{1,2}):([0-9]{1,2}):[0-9]{1,2}"
```

Ici, nous demandons l'heure de l'exfiltration, mais plusieurs journaux
enregistrent l'exfiltration à des secondes différentes. Les secondes n'étant
pas significatives car la période de log donnée est sur plus d'une journée, on
peut considérer qu'un participant qui donne la bonne heure et la bonne minute a
répondu à la question. On le laisse cependant recopier le timestamp complet
pour qu'il fasse l'effort de trouver la bonne valeur selon lui (il pourrait
être tenté de ne pas aller au bout de la démarche si on lui demande directement
une valeur approximative).

On garde toujours dans le `raw` la valeur complète attendue, telle que l'on
peut la recopier. Et l'on indique une `capture_regexp` qui sélectionnera la
ou les valeurs significatives, à la fois dans le `raw` et dans les réponses des
participants.

La valeur qui sera hashée pour comparaison sera `1122`, ce qui correspond à la
concaténation de tous les groupes de notre expression rationnelle.

Il est préférable d'écrire une expression rationnelle vague, qui ne fait pas
apparaître les termes de la réponse. Ici, nous aurions pu utiliser
l'expression rationnelle suivante : `(11):(22):(?:33|34|35)`, mais cela aurait
fait apparaître clairement les termes de la réponse. L'attribut étant parfois
transmis aux participants pour être utilisé par l'interface, il est prérérable
de rester vague, en utilisant les caractères joker. Par exemple :

```toml
[[flag]]
label = "Commande utilisée pour l'exfiltration"
raw = 'apt-get search yolo'
capture_regexp = "^(?:sudo *)?(.*)$"
```

Dans cet exemple `sudo` est facultatif, plutôt que de sélectionner `(apt-get
search yolo)$`, on exclut plutôt `sudo`, en le plaçant dans un groupe
non-capturant (`?:`), ce qui ne révèle pas d'information.

## Propriétés

id
: (facultatif) identifiant du flag au sein de l'étape, pour définir des dépendances ;

label
: (facultatif, par défaut : `Flag`) intitulé du drapeau ;

raw
: drapeau exact à trouver ; sous [forme de tableau]({{% relref "/responses/vector.md" %}}), le participant n'aura pas connaissaance du nombre d'éléments ;

capture_regexp
: (facultatif) [expression rationnelle]({{% relref "/responses/simple.md#flag-modulable" %}}) dont les groupes capturés serviront comme chaîne à valider (notez que `?:` au début d'un groupe ne le capturera pas) ;

sort\_capture\_regexp\_groups
: (facultatif, par défaut : `false`) trie les groupes capturant par ordre alphabétique avant concaténation ;

casesensitive
: (facultatif, par défaut : `false`) prend en compte la casse de ce drapeau lors de la validation ;

placeholder
: chaîne de caractères placée dans le champ du formulaire avant la frappe, idéale pour donner un exemple de ce qui serait attendu ou une indication de format ;

unit
: (facultatif) chaîne de caractères ajoutée à la fin du champ pour indiquer une information complétant le flag, par exemple l'unité de mesure attendue ;

help
: (facultatif, rarement utilisé) chaîne de caractères placée sous le champ du formulaire, idéale pour donner un détail supplémentaire. Cette propriété peut contenir du [Markdown](https://commonmark.org/).

bonus_gain
: (facultatif) Voir [flag bonus]({{% relref "/responses/bonus.md" %}}).


## Rendu

![Rendu flag simple](flag.png)
