---
date: 2019-04-05T15:59:52+02:00
title: Liste de flags
weight: 7
---

Ce type de flag s'utilise pour valider plusieurs chaînes de caractères liées au même flag.
Par exemple, s'il faut trouver une liste d'administrateurs, les IP appartenant à un botnet, les fichiers infectés, ...

Attention, par défaut, les propositions ne sont pas sensibles à la casse[^gocase].

[^gocase]: Casse selon les classes Unicode de caractères, cela ne concerne pas
    que l'alphabet traditionnel. Pour plus d'infos, consultez la [documentation
    de la fonction utilisée](https://golang.org/pkg/bytes/#ToLower).

### Exemple

```toml
[[flag]]
label = "IPv6 du botnet"
raw = ['fe80::319c:1002:7c60:68fa', 'fe80::319c:1002:7c60:68fb', 'fe80::319c:1002:7c60:68fc', 'fe80::319c:1002:7c60:68fd' ]
```

Il convient de lister toutes les IP du botnet, l'ordre importe peut, donc on ne précise pas la propriété `ordered` ; la casse d'un IP n'a pas non plus d'importance, donc on laisse la propriété `casesensitive` à sa valeur par défaut.

## Propriétés

type
: `"vector"`

id
: (facultatif) identifiant du flag au sein de l'exercice, pour définir des dépendances ;

label
: (facultatif, par défaut : `Flag`) intitulé du drapeau ;

raw
: tableau TOML de drapeaux exacts à trouver ; sous forme de tableau, le participant n'aura pas connaissaance du nombre d'éléments ;

capture_regexp
: (facultatif) [expression rationnelle]({{% relref "/responses/simple.md#flag-modulable" %}}) dont les groupes capturés serviront comme chaîne à valider (notez que `?:` au début d'un groupe ne le capturera pas). Attention, la regexp est appliquée seulement sur la représentation de la chaîne de caractères obtenue, pas sur chaque élément ;

showlines
: (facultatif, restreint aux vecteurs de moins de 10 éléments, par défaut : `false`) affiche directement le bon nombre de champs de réponse dans l'interface, plutôt que de laisser la taille du tableau inconnue aux participants ;

ordered
: (facultatif, par défaut : `false`) ignore l'ordre dans lequel les éléments du tableau sont passés ;

casesensitive
: (facultatif, par défaut : `false`) prend en compte la casse des drapeaux lors de la validation ;

placeholder
: chaîne de caractères placée dans le champ du formulaire avant la frappe, idéale pour donner un exemple de ce qui serait attendu ou une indication de format ;

unit
: (facultatif) chaîne de caractères ajoutée à la fin du champ pour indiquer une information complétant le flag, par exemple l'unité de mesure attendue ;

help
: (facultatif, rarement utilisé) chaîne de caractères placée sous le champ du formulaire, idéale pour donner un détail supplémentaire.

separator
: (facultatif, par défaut `,`) caractère utilisé comme séparateur lors de la fusion des différents éléments de réponse. Si l'une des valeurs de la réponse contient `,`, remplacez le séparateur par n'importe quel caractère qui n'est contenu dans aucune des chaînes.


## Rendu

![Rendu list de flag](flag.png)

Dans cet exemple, on demande explicitement que les IP soient listées dans l'ordre, on aura pris soin d'ajouter `ordered = true`.
