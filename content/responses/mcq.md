---
date: 2019-04-05T15:59:52+02:00
title: QCM
weight: 10
---

Ce type de flag est approprié pour suggérer aux participants différentes propositions parmi lesquelles il devra déterminer si elles sont vraies ou fausses.

Lors de la résolution de ce type de flag, le nombre de différences d'état des cases à cocher est affiché (type Mastermind).


### Exemple

```toml
[[flag]]
type = "mcq"
label = "Fichiers exfiltrés :"

    [[flag.choice]]
    label = "Documents PDF"

    [[flag.choice]]
    label = "Tableurs Excel"
    value = true

    [[flag.choice]]
    label = "Image PNG"
    value = true
```

## Propriétés

type
: `"mcq"`

id
: (facultatif) identifiant du flag au sein de l'exercice, pour définir des dépendances ;

label
: (facultatif, par défaut : `Flag`) intitulé du questionnaire ;

noshuffle
: (facultatif, par défaut : `false`) conserve l'ordre des propositions, au lieu de les mélanger ;

help
: (facultatif) chaîne de caractères placée près du formulaire, idéale pour donner une indication de format (pas encore implémenté faute d'usage concret, n'hésitez pas à demander si vous avez un usage).


## Propriétés des choix

id
: (facultatif) identifiant du flag au sein de l'exercice, pour définir des dépendances ;

label
: intitulé du choix ;

value
: (facultatif, par défaut : `false`) état attendu de la case à cocher pour ce choix (`true` = coché, `false` = décoché).


## Rendu

![Rendu flag QCM](flag.png)
