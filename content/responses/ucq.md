---
date: 2019-04-05T15:59:52+02:00
title: Liste de choix
weight: 20
---


Ce type de flag s'utilise pour valider une réponse que le participant aurait du mal à écrire correctement ou à déterminer sans avoir plus d'indications. C'est notamment le cas pour les numéros de CVE (la liste de choix permet alors de recentrer les recherches à effectuer), ou les types de *payloads* : comme chacun peut avoir sa façon d'écrire ou de nommer une charge malveillante.

## Propriétés

type
: `"ucq"` ou `"radio"` (les `ucq` se présentent sous la forme d'une liste de choix, élément `<select>`, tandis que `radio` présente le flag sous la forme de boutons radios) ;

id
: (facultatif) identifiant du flag au sein de l'exercice, pour définir des dépendances ;

label
: (facultatif, par défaut : `Flag`) intitulé du drapeau ;

raw
: drapeau exact à trouver ; forme de [tableau]({{% relref "/responses/vector.md" %}}) possible, mais déconseillée ;

choices_cost
: (facultatif, par défaut 0) coût pour afficher les choix : avant l'affichage, se comporte comme un `flag` classique (si `choices_cost` = 0, les choix sont affichés directement) ;

capture_regexp
: (facultatif) expression rationnelle dont les groupes capturés serviront comme chaîne à valider (notez que `?:` au début d'un groupe ne le capturera pas) ;

ordered
: (facultatif, par défaut : `false`) ignore l'ordre dans lequel les éléments du tableau sont passés ;

casesensitive
: (facultatif, par défaut : `false`) prend en compte la casse de ce drapeau lors de la validation ;

placeholder
: chaîne de caractères placée dans le champ du formulaire avant la frappe, idéale pour donner un exemple de ce qui serait attendu ou une indication de format ;

unit
: (facultatif) chaîne de caractères ajoutée à la fin du champ pour indiquer une information complétant le flag, par exemple l'unité de mesure attendue ;

help
: (facultatif, rarement utilisé) chaîne de caractères placée sous le champ du formulaire, idéale pour donner un détail supplémentaire.


## Propriétés des choix

label
: (facultatif, par défaut `value` est utilisé) intitulé du choix ;

value
: valeur envoyée par le formulaire de validation lorsque le choix est sélectionné.


### Exemple

```toml
[[flag]]
type = "ucq"
label = "Type de malware"
capture_regexp = "^.*(dropper).*$"
raw = "Il installe quelque chose à son tour ! Il s'agit d'un dropper."
choices_cost = 25

    [[flag.choice]]
    value = "Il exécute arbitrairement une commande passée en paramètre par un botnet !"

    [[flag.choice]]
    value = "Un évènement est déclenché à un temps précis ! Il s'agit d'un logic-bomb !"

    [[flag.choice]]
    value = "Il supprime tout dans le disque."

    [[flag.choice]]
    value = "Il installe quelque chose à son tour ! Il s'agit d'un dropper."

    [[flag.choice]]
    value = "Il communique avec un autre ordinateur à distance."
```


## Rendu

![Rendu flag UCQ](flag.png)


### Rendu avec `choices_cost > 0`

![Rendu flag UCQ](flag_locked.png)

Le participant a la possibilité d'écrire lui-même le type de malware dans un premier temps, mais s'il n'y arrive pas, il peut dépenser 25 points pour obtenir une liste de choix.
