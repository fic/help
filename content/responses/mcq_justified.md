---
date: 2019-04-05T15:59:52+02:00
title: QCM justifiés
weight: 15
---

Ce type de flag est préconisé pour orienter les participants lorsqu'il faut trouver beaucoup de flags. Il devra déterminer si chacune est vraie ou fausse, puis pour les propositions qu'il juge vraies, apporter un élément de justification.

Lors de la résolution de ce type de flag, le nombre de différences d'état des cases à cocher est affiché (type Mastermind).

La résolution de ce flag peut se faire en deux étapes : lorsque le participant a trouvé tous les choix vrais dans le QCM (sans pour autant avoir les bonnes justifications), les choix sont alors verrouillés et des informations supplémentaires sur les justifications sont affichées.

### Exemple

```toml
[[flag]]
type = "justified"
label = "Fichiers exfiltrés :"

    [[flag.choice]]
    label = "Documents PDF"

    [[flag.choice]]
    label = "Tableurs Excel"
    raw = "Salaires09.xls"
    placeholder = "Indication utile si besoin"
    casesensitive = true

    [[flag.choice]]
    label = "Image PNG"
    raw = "heading.png"
    casesensitive = true
```


## Propriétés

type
: `"justified"`

id
: (facultatif) identifiant du flag au sein de l'exercice, pour définir des dépendances ;

label
: (facultatif, par défaut : `Flag`) intitulé du questionnaire ;

noshuffle
: (facultatif, par défaut : `false`) conserve l'ordre des propositions, au lieu de les mélanger ;

help
: (facultatif) chaîne de caractères placée près du formulaire, idéale pour donner une indication de format (pas encore implémenté faute d'usage concret, n'hésitez pas à demander si vous avez un usage).


## Propriétés des choix

id
: (facultatif) identifiant du flag au sein de l'exercice, pour définir des dépendances ;

label
: intitulé du choix ;

raw
: [drapeau]({{% relref "/responses/simple.md" %}}) ou [tableau de drapeaux]({{% relref "/responses/vector.md" %}}) exact à trouver ;

capture_regexp
: (facultatif) [expression rationnelle]({{% relref "/responses/simple.md#flag-modulable" %}}) dont les groupes capturés serviront comme chaîne à valider (notez que `?:` au début d'un groupe ne le capturera pas) ;

casesensitive
: (facultatif, par défaut : `false`) prend en compte la casse de ce drapeau lors de la validation ;

ordered
: (facultatif, par défaut : `false`) ignore l'ordre dans lequel les éléments du tableau sont passés ;

placeholder
: chaîne de caractères placée dans le champ du formulaire avant la frappe, idéale pour donner un exemple de ce qui serait attendu ou une indication de format ;

help
: (facultatif, rarement utilisé) chaîne de caractères placée sous le champ du formulaire, idéale pour donner un détail supplémentaire.



## Rendu

![Rendu flag QCM justifié](flag.png)
