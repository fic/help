---
date: 2019-04-04T15:59:52+02:00
title: AUTHORS.txt
weight: 10
---

Le fichier `AUTHORS.txt`, à la racine de chaque dossier de scénario, contient vos noms, tels qu'ils apparaîtront sur la plate-forme.

## Format du fichier

Chaque ligne contient le nom qui apparaîtra sur le site, suivi, éventuellement d'un [lien hypertexte complet](https://tools.ietf.org/html/rfc2396), placé entre chevrons. Ce lien peut être, par exemple, l'adresse du blog de l'auteur correspondant, son adresse électronique, profil LinkedIn, ...


{{% notice warning %}}
Attention : après le FIC, vos challenges seront publiés et accessibles **publiquement** aux côtés [des challenges des années précédentes](https://fic.srs.epita.fr/).
Cela signifie que l'identité que vous indiquez dans ce fichier sera publique et indexée par les moteurs de recherche.
Nous vous recommandons de ne garder que l'initiale de votre prénom ou de votre nom, afin qu'une recherche ciblant votre identité n'aboutisse pas sur ce site.
{{% /notice %}}


### Exemple

```text
Courtois J. <mailto:courto_j@epita.fr>
Bombal S.
Mercier P-O. <https://nemunai.re/>
```

### Rendu

![Rendu AUTHORS.txt](screenshoot.png)
