---
date: 2022-10-29T18:53:44+02:00
title: finished.md
weight: 34
---

Remplissez ce fichier optionnel, lorsque vous souhaitez apporter une information aux participants une fois qu'ils ont validé cette étape.

Comme l'ensemble des textes importés, vous pouvez utiliser du [Markdown](https://commonmark.org/) pour mettre en forme vos textes.


### Rendu

![Rendu finished.md](exercice_finished.png)
