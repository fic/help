---
date: 2019-04-04T15:59:52+02:00
title: Arborescence et fichiers
weight: 10
---

Afin de pouvoir être importé automatiquement sur la plate-forme, vos scénarios
doivent respecter une certaine arborescence que voici :

```
    .
    ├── AUTHORS.txt
    ├── overview.md
    ├── title.txt
    ├── heading.jpg
    ├── repochecker-ack.txt
    ├── CHID-Titre de l'étape/
    │   ├── challenge.toml
    │   ├── finished.md    (opt.)
    │   ├── heading.jpg    (opt.)
    │   ├── links.txt
    │   ├── overview.md
    │   ├── resolution.md   (choice)
    │   ├── resolution.mp4  (choice)
    │   ├── statement.md
    │   ├── files/
    │   │   ├── DIGESTS.txt
    │   │   └── ...
    │   └── ressources/
    │       └── ...
    │
    ├── CHID-Titre de l'étape/
    │   └── ...
    └── ...
```

Ou pour un exercice seul :

```
    .
    ├── AUTHORS.txt
    ├── challenge.toml
    ├── finished.md    (opt.)
    ├── heading.jpg
    ├── links.txt
    ├── overview.md
    ├── resolution.md   (choice)
    ├── resolution.mp4  (choice)
    ├── repochecker-ack.txt
    ├── statement.md
    ├── title.txt
    ├── files/
    │   ├── DIGESTS.txt
    │   └── ...
    └── ressources/
        └── ...
```

{{% notice warning %}}
Pour le bon usage et la bonne configuration de vos dépôts sur GitLab, veuillez consulter [la page dédiée]({{<relref "git">}}).
{{% /notice %}}

{{% notice info %}}
N'ajoutez pas inutilement de dossiers ou fichiers vides. Ceux-ci doivent sans doute être optionnels et risquent de vous/nous induire en erreur lors de nos vérifications.
{{% /notice %}}

Utilisez l'image Docker [nemunaire/fic-repochecker](https://hub.docker.com/r/nemunaire/fic-repochecker) pour vous assurer de votre arborescence et de la validité du contenu des fichiers :

    cd workspace/fic/MyTheme
    docker container run -v $(pwd):/mnt/fic/ nemunaire/fic-repochecker /mnt/fic


### `CHID`

Dans les noms de dossiers, `CHID` correspond à un identifiant permettant de référencer votre étape (pour déclarer une dépendance sur celle-ci par exemple).

L'import s'effectuant selon l'ordre alphabétique, vous devriez utiliser le numéro d'ordre de l'étape comme identifiant :

```
    .
    ├── 1-Titre du premier défi/
    │   └── ...
    ├── 2-Titre du deuxième défi/
    │   └── ...
    └── ...
```

Dans cet exemple, si l'ordre était déterminé uniquement par l'ordre alphabétique des noms de dossiers, le **d**euxième serait devant le **p**remier (`d<p`).
Ajouter le numéro d'ordre permet de contrôler plus facilement l'ordre des étapes, sans que cela soit affiché dans l'interface.
