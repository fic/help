---
date: 2021-10-25T09:48:00+02:00
title: resolution.md
weight: 24
---

Ce fichier doit contenir les explications détaillées permettant la résolution
de l'étape. Si vous pensez qu'une vidéo est plus appropriée, vous pouvez remplacer
ce fichier par [`resolution.mp4`]({{% relref "resolution" %}})

Votre article doit respecter les caractéristiques suivantes :

- être écrit en [Markdown](https://spec.commonmark.org/0.30/) et peut être
  complété par [des extensions
  courantes](https://github.com/russross/blackfriday/tree/v2#extensions) ;
- être rédigé en français (préféré) ou en anglais ;
- être structuré avec un titre et des sections (une section par grande étape) ;
- contenir au moins 1 capture d'écran par étape de résolution : pour quelqu'un
  qui connaît les outils, la seule analyse de vos captures d'écran devrait
  permettre de reproduire chaque étape ;
- le texte, présenté avant l'image correspondante, doit expliquer et détailler
  la démarche d'avancée dans la résolution.

N'hésitez pas à encadrer ou à mettre en valeur les flags ou les éléments
importants sur les captures d'écran.
N'hésitez pas non plus à donner des liens vers des articles techniques expliquant
plus en détail vos techniques. Insérez plutôt ces liens au sein de la section
concernée plutôt qu'à la fin.

### Captures d'écran et image

Vous pouvez insérer des images comme ceci :

    ![alt](path)

Vous *pouvez* mettre vos images dans un sous-dossier `resolution/` pour ne pas
encombrer le dossier de l'étape.

## Rendu

{{% notice note %}}Il s'agit d'une nouvelle fonctionnalité, le rendu est encore
à l'étude.
{{% /notice %}}
