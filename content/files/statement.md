---
date: 2022-10-29T18:54:43+02:00
title: statement.md
weight: 18
---

Le fichier `statement.md` (ou `statement.txt`) est l'énoncé de l'étape, tel qu'il sera affiché sur le site, à destination des participants.

Il doit bien poser le contexte, expliquer notamment pourquoi on est là, qu'est-ce qui justifie la démarche qui sera effectuée lors de l'étape, au sein de l'entreprise.

N'oubliez pas de donner des objectifs clairs sur ce que l'on attend des participants avec les fichiers que vous fournissez.
Restez tout de même suffisamment évasifs pour ne pas dévoiler involontairement des éléments demandés.

Comme l'ensemble des textes importés, vous pouvez utiliser du [Markdown](https://commonmark.org/) pour mettre en forme vos textes.


### Rendu

![Rendu statement.md](exercice_statement.png)
