---
date: 2022-10-29T19:05:05+02:00
title: resources/
weight: 40
---

Rangez dans ce dossier toutes les ressources et scripts que vous avez utilisé pour réaliser l'étape : pour sa construction ou sa résolution, les schémas du SI au premier étape concerné.

Ajoutez éventuellement un `README.txt` avec les liens des outils externes.

Ce dossier n'a pas vocation à être diffusé publiquement.
