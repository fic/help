---
date: 2019-04-04T15:59:52+02:00
title: resolution.mp4
weight: 25
---

Ce fichier doit contenir la vidéo permettant la résolution du défi,
étape par étape, chaque partie devant être accompagnée d’explications.
Si vous pensez qu'un article est plus approprié, vous pouvez remplacer
ce fichier par [`resolution.md`]({{% relref "write-up" %}})

Ce fichier doit contenir la vidéo de résolution, montée selon ces caractéristiques :

- format MP4 :
	- codec vidéo H.264,
	- codec audio AAC (factultatif, uniquement si vous avez de l'audio),
	- piste de sous-titre 3GPP Timed Text (**NE PAS incruster les sous-titres** à la piste vidéo, les sous-titres **DOIVENT** être sur une piste séparée) ;
- utiliser les sous-titres pour commenter les étapes ; pas de commentaires audio
- environ 2' par vidéo : maxi 1'30" pour les challenges simples, 3-4' maxi
- résolution 1080p maximum (il faut faire en sorte que la solution soit lisible sur des écrans relativements petits, n'hésitez pas à agrandir la police de vos terminaux et applications)

## Exemple de logiciel de capture d'écran

{{% notice warning %}}
N'utilisez pas de logiciel en version de « démonstration » : il faudrait payer la licence pour publier la vidéo.
{{% /notice %}}

- `ffmpeg -video_size 1920x1080 -framerate 25 -f x11grab -i :0.0 -f alsa -ac 2 -i hw:0 -strict experimental resolution.mp4`
- [recordMyDesktop](http://recordmydesktop.sourceforge.net/) sous Linux
- [Screencast Capture Lite](http://cesarsouza.github.io/screencast-capture/) pour Windows
- [OBS](https://obsproject.com/) sur tous les systèmes
- [VTT Creator](https://www.vtt-creator.com/editor), [Gnome Subtitle](http://gnomesubtitles.org/), emacs/vim, ... pour les sous-titres


## Rendu

Les vidéos de résolution sont affichées uniquement sur la version du site statique, publiée après le challenge.

![Rendu resolution.mp4](exercice_resolution.png)

## Tuto de création de vidéo de résolution

{{% notice note %}}
Section contribuée par Mahé Charpy.
{{% /notice %}}

### Filmer la résolution

Pour filmer la résolution, OBS est pas mal. Les autres solutions proposées au-dessus fonctionneront également

N'hésitez pas à retirer du cadre les informations non essentielles (`i3-nagbar` par exemple)

N'hésitez pas à faire cette résolution dans votre `/tmp` afin que les chemins de fichiers ne présentent que les informations nécessaires

### Monter la vidéo

Sur Linux, OpenShot ([https://www.openshot.org/](https://www.openshot.org/)) est pas mal pour ça. Sur Arch vous pouvez l'obtenir avec la commande

```bash
yay -S openshot
```

Vous pouvez ensuite lancer openshot-qt pour ouvrir le logiciel de montage

![Interface OpenShot](interface_openshot.png)


Importer vos vidéos

![Importer des vidéos](openshot_import_video.png)

Placer vos vidéos sur la barre de montage

![Le montage des vidéos](openshot_montage.png)

Vous pouvez maintenant découper et réorganiser la vidéo comme vous le souhaitez.

Pensez à enregistrer régulièrement (ctrl + s) car openshot demande pas mal de RAM et il arrive que le noyau passe par l'OOM killer (#virli) pour récupérer cette RAM

Une fois le montage fait, exporter la vidéo (ctrl + e)


#### 3 raccourcis sympathiques sur openshot

- ctrl + j : Coupe la bande vidéo au niveau du curseur de lecture et ne garde que ce qui se trouve après le curseur de lecture
- ctrl + k : Coupe la bande vidéo au niveau du curseur de lecture et garde les 2 cotés
- ctrl + l : Coupe la bande vidéo au niveau du curseur de lecture et ne garde que ce qui se trouve avant le curseur de lecture

### Les sous-titres

Pour faire les sous-titres, nous allons passer par un fichier `.srt`

```bash
vim sous_titres.srt
```

Dans vim, faire la suite de touches suivante

```bash
99o<Echap>:%s/^/\=printf("%d\\n --> 00:00:00,0000\\n\\n", line('.'))<Entree>
:%s/\\n/\r/g<Entree>
:%s/^1\n --> 00:00:00,0000/1\r00:00:00,0000 -> 00:00:00,0000<Entree>
```

Explication des commandes :

- `99o<Echap>` ajoute 99 lignes vides au fichier. On obtient un fichier de 100 lignes vides
- `:%s/^/\=printf("%d\\n --> 00:00:00,0000\\n\\n", line('.'))`  met la string `%d\n --> 00:00:00,0000\n\n` sur chaque ligne en remplaçant le %d par le numero de ligne
- `:%s/\\n/\r/g` remplace les `\n` par des vrais retours a la ligne
- `:%s/^1\n --> 00:00:00,0000/1\r00:00:00,0000 -> 00:00:00,0000` Ajoute un timestamp de debut a la première entrée


Vous devriez maintenant avoir un fichier (presque) au format d'un fichier de sous-titres avec 100 entrées

![Le fichier obtenu par les commandes précédentes](sous_titres_debut.png)


Ce fichier n'a pas le format d'un fichier de sous-titre car les lignes de timestamp n'ont pas de timestamp de début. Nous l'ajouterons à la fin, une fois que tous les timestamps de fin auront été définis.

Remplir le fichier de sous-titres en définissant bien les timestamps de fin d'apparition du sous-titre

![Les possibilités de sous-titres et timestamps](sous_titres_exemple.png)

Une fois les sous-titres finis, supprimer les entrées non complétées restantes et utiliser la commande vim suivante

```bash
:%s/-> \([0-9:,]*\)0\n\(\_.\{-}\)\n -/-> \10\r\2\r\11 -/g
```

Cette commande a pour effet d'ajouter les timestamps de début de sous-titre. Pour cela, on récupère le timestamp de fin du sous-titre précédent et on y ajoute 1 milliseconde afin que les sous-titres ne se chevauchent pas

Attention, il faut que les timestamps soient tous à un nombre de millisecondes multiple de 10

On obtient un fichier avec cette forme

![Résultat de la commande](sous-titres_final.png)

Vous pouvez vérifier que les sous-titres vous conviennent en ouvrant la vidéo dans VLC et en y ajoutant les sous-titres

![Afficher des sous-titres sur VLC](vlc_sous_titres.png)

Une fois que vous êtes contents de vos sous-titres, ajouter les sous-titres directement en tant que piste de sous-titre dans la video avec la commande suivante :

```bash
ffmpeg -i votre_video.mp4 -i sous_titres.srt -c copy -c:s mov_text resolution_subtitled.mp4
```
