---
date: 2022-05-08T15:59:52+02:00
title: Consignes pour Git
weight: 5
---

Lorsque vous mettez en place vos dépôts Git sur la forge, il est important de respecter un certain nombre de consignes.

## Nommage du dépôt Git

Afin de standardiser l'accès à vos différents dépôts, ceux-ci doivent avoir l'URL suivante :

    https://gitlab.cri.epita.fr/ing/majeures/srs/fic/YEAR/GRPID/scenario-X.git

Où :

- `YEAR` est l'année de votre promotion ;
- `GRPID` est le numéro de votre groupe au sein du projet FIC ;
- `scenario` est à remplacer par `exercice` s'il s'agit d'un exercice indépendant ;
- `X` est le numéro de votre scénario ou exercice en commençant par 1.

Par exemple :

    https://gitlab.cri.epita.fr/ing/majeures/srs/fic/2042/08/scenario-1.git


{{% notice note %}}
Renseignez par contre le nom de votre scénario/exercice (tel qu'il apparaît sur la plateforme) dans le champ *Project name*. C'est seulement le *Project slug* qui doit être standardisé, cela vous permet de changer de nom sans changer de dépôt. C'est à la fois plus simple pour que l'on recherche et que l'on accéde à vos dépôts.
{{% /notice %}}


## Convention de branches

Votre dépôt doit contenir au minimum ces deux branches :

- `master` : c'est votre branche stable, sur laquelle votre travail est considéré terminé, relu et définitif. Elle est synchronisée avec la plateforme sous l'année de promo seulement.
- `dev` : c'est votre branche principale de travail, vos modifications sont considérées stables (dans le sens où l'import sur la plateforme ne doit pas être cassé), mais votre travail pas nécessairement terminé ou pas parfaitement relu. Elle est également synchronisée sur la plateforme, mais sous l'année de promo + `-dev` (par exemple `/2042-dev/`).

Vous êtes libre d'utiliser toutes les branches dont vous avez besoin. Néanmoins, deux branches ont un rôle particulier pour le système de synchronisation.


## Où mettre le code source de mes binaires ?

Vous pouvez créer sur GitLab des dépôts pour vos différents projets liés à vos scénarios ou exercices. N'hésitez pas à créer des dépôts à côté des dépôts de vos exercices.

S'il s'agit d'un fichier ou d'un petit groupe de fichiers pour lequel créer un dépôt n'a pas d'intérêt, vous pouvez utiliser le dossier `resources` qui est présent dans chaque dossier d'exercice (ou d'étape de scénario).

Il est important de NE PAS déclarer de sous-module (*submodule*) vers des dépôts externes à gitlab et de ne pas utiliser le dossier `resources` pour faire un lien vers des dépôts que l'on peut retrouver à côté du dépôt de votre scénario/exercice. Cela alourdi inutilement le processus de synchronisation.


## AUCUN FICHIER BINAIRE dans l'historique

Git est un gestionnaire de version qui gère particulièrement bien les fichiers texte, comme peuvent l'être les fichiers de code source.

Lorsqu'il est confronté à des fichiers binaires, Git perd toute son utilité car il est incapable de voir les différences entre ces fichiers.
Il en résulte qu'en cas de modification d'une image, d'un PCAP ou similaire, l'historique du dépôt est pollué par des gros bouts d'octets sans intérêt.

Pour ne pas dégrader les performances de vos dépôts sur GitLab, vous DEVEZ utiliser LFS pour stocker vos fichiers binaires. De cette manière, ces fichiers, seront gérés par un processus de stockage différent (ils seront stockés sur S3 qui est capable de gérer une grosse quantité de gros fichiers, contrairement au volume RBD des dépôts Git).

Si vous pushez par mégarde un binaire sur vos dépôts, vous devrez sans doute le recréer de zéro. Donc faites attention.


## Réponse et fermeture des tickets

Lorsqu'un ticket est ouvert et nécessite un travail ou une correction, vous devez lier la clôture du ticket au commit ou à la demande de fusion correspondant. Cela permet de mieux suivre les modifications et de faire les revues.

GitLab (comme GitHub et la majorité des forges à vrai dire) dispose d'un mécanisme permettant facilement d'indiquer dans le message de commit la relation avec un ticket ouvert : <https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically>.
