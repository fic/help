---
date: 2022-10-29T19:05:40+02:00
title: title.txt
weight: 14
---

# Dépôt de scénario

Ce fichier peut être utilisé pour y inscrire le nom du scénario, tel qu'il doit apparaître sur l'interface.


# Dépôt d'exercice indépendant

Ce fichier peut être utilisé pour donner le nom de l'exercice, tel qu'il doit apparaître sur l'interface.
