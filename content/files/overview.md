---
date: 2022-10-29T18:43:59+02:00
title: overview.md
weight: 12
---

Le fichier `overview.md` (ou `overview.txt`) est une présentation du contexte diffusée publiquement, qui doit être compréhensible par un décideur.

Il existe 1 fichier par scénario ainsi qu'1 fichier par étape. Pour le scénario, il s'agit de présenter le contexte du scénario, tandis que pour l'étape, il s'agit de présenter le contexte général de l'étape, ce qui va se passer. Le tout, sans jamais dévoiler ou donner d'indices sur des éléments d'une autre étape (puisque les `overview` sont publiques et accessibles de tous).


## Scénario

Le fichier `overview.md` est une présentation du contexte du scenario (~2-3 paragraphes + 1 paragraphe d'accroche), compréhensible par un décideur, avec petit schéma à l'appui.

Le fichier **doit** comporter un paragraphe d'accroche (qui sera affiché plus gros que les suivants). Celui-ci correspond à la première **ligne** de votre fichier.

Comme l'ensemble des textes importés, vous pouvez utiliser du [Markdown](https://commonmark.org/) pour mettre en forme vos textes.

Vous pouvez insérer des images dans tous les textes :

    ![alt](path title)


### Rendu

![Rendu overview.txt](scenario_overview_public.png)

![Rendu overview.txt](scenario_headline.png)


## Étape

Une présentation rapide de l'étape (~1-2 phrases), compréhensible par un décideur, petit schéma à l'appui si besoin.

Le fichier **doit** comporter une phrase d'accroche (qui sera affichée plus grosse que les suivantes). Celle-ci correspond à la première ligne de votre fichier.

Comme l'ensemble des textes importés, vous pouvez utiliser du [Markdown](https://commonmark.org/) pour mettre en forme vos textes.

Gardez en tête que les *overview*, que ce soit scénario ou étapes, sont affichées au public : il s'agit à la fois d'aguicher le participant pour qu'il fasse votre scénario plutôt que celui d'un autre groupe, mais aussi de donner
envie au public de lire plus en détail.


### Rendu

![Rendu overview.txt](exercice_overview_public.png)

![Rendu overview.txt](exercice_headline.png)
