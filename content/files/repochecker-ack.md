---
date: 2022-10-29T19:14:37+02:00
title: repochecker-ack.txt
weight: 16
---

Dans le fichier `repochecker-ack.txt` se trouvant à la racine de votre dépôt, vous pourrez indiquer les faux-positifs retournés par `repochecker`.

{{% notice info %}}
Toutes les erreurs remontées par `repochecker` ne bénéficient pas d'un mécanisme d'aquitement des faux-positifs.
Lorsqu'il peut s'agir d'un faux-positif, `repochecker` indique à la fin de la ligne d'erreur `(:anti-false-positive-string)`.
{{% /notice %}}

`repochecker` est susceptible de retourner des erreurs telles que la suivante :

```
5-a-never-ending-cycle: overview.md: Confusion. La lieue est une unité de distance. Les lieus sont des poissons. (1:g3__conf_avoir_donner_lieu__b2_a1_1)
"Une intervention malicieus a bien eue lieue\u00a0!"
                                       ^^^^^
Suggestion : lieu
```

L'indication est donnée sur l'étape de scénario concernée (ici `5-a-never-ending-cycle`), sur le fichier dans lequel se trouve ce texte (ici `overview.md`).
Grammalecte détaille ensuite la faute avec un message explicite. La ligne se termine par un identifiant entre parenthèses (ici `1:g3__conf_avoir_donner_lieu__b2_a1_1`), dont on verra l'usage par la suite.


## Traiter les faux positifs grammaticaux

Il arrive parfois que Grammalecte se trompe sur une correction grammaticale.

Par exemple :

```
3-L'ascension de skyhacker​/challenge.toml: flag#4: help Il manque un espace. (1:typo_espace_manquant_après3)
"prénom.nom"
        ^^^
Suggestion :  nom
```

Pour ignorer cette erreur, on indique dans le fichier `repochecker-ack.txt` :

```
step-path/filename.md:paragraph-id:rule-id
```

Avec `step-path/` **l'éventuel chemin** pour atteindre le fichier concerné depuis `repochecker-ack.txt` ; `filename.md` le nom du fichier dans lequel se trouve l'erreur, `paragraph-id` le numéro de paragraphe dans le fichier (commençant à 1) et `rule-id` l'identifiant de la règle Grammalecte que l'on souhaite ignorer. L'identifiant entre parenthèse à la fin de la première ligne renseigne sur le `paragraph-id:rule-id`

Dans notre exemple, cela donnerait :

```
3-L'ascension de skyhacker​/challenge.toml:1:typo_espace_manquant_après3
```

Dans un dépôt d'exercice, ce serait :

```
challenge.toml:1:typo_espace_manquant_après3
```


## Traiter les faux positifs orthographiques

Il arrive parfois que Grammalecte ne connaisse pas un mot ou un terme que vous employez (`spelling error`) : cela peut être le nom d'un société factice, d'une équipe, d'une machine, ...

Par exemple :

```
grp4-scenario1: overview.md: spelling error WORD "InformaSup" (:spelling:InformaSup)
"ALERTE\u00a0: InformaSup, une nouvelle école d’informatique en plein cœur de la technologie est victime d’une attaque. Elle demande l’aide d’une équipe experte dans le domaine de la cybersécurité pour les aider à sortir cette situation."
          ^^^^^^^^^^
Suggestions : Informas pu
```

Ici le nom de l'entreprise n'est pas connu du correcteur orthographique, on peut donc sans problème ignorer ces erreurs pour tous les fichiers du scénario (puisque l'entreprise sera sans doute nommée dans de nombreux fichiers, pas seulement l'`overview.md`).

On indique alors dans le fichier `repochecker-ack.txt` :

```
*:spelling:InformaSup
```

`*` désigne tous les fichiers (mais vous pouvez ignorer l'erreur que sur un fichier en particulier), `spelling:InformaSup` est l'identifiant de l'erreur tel que `repochecker` nous le donne.

{{% notice warning %}}
Nous vous demandons d'être le plus précis possible et de limiter l'usage du `*` aux cas strictement employés à de nombreuses reprises dans vos textes.
Lorsqu'il s'agit d'un faux-positif sporadique, précisez de quel fichier il s'agit.
Lorsque nous passerons en revu votre dépôt, nous vérifierons les faux-positifs que vous avez indiqués pour nous assurer que vous n'avez pas ajouté de non-faux-positif.
Nous devrons pour cela être capable d'aller voir rapidement le/les fichiers concernés.
Il n'est pas question pour nous de devoir partir à la recherche du mot dans tous les fichiers.
{{% /notice %}}


## Gérer les passages en anglais

Dans les fichiers Markdown uniquement, vous pouvez avoir des passages entiers en anglais : il peut s'agir par exemple d'une citation.

Dans ce cas vous n'êtes pas obligés de marquer chaque mot, individuellement, comme ayant une orthographe correcte, mais vous pouvez utiliser :

```
statement.md:quote:Your quote
```

Dans cet exemple, « *Your quote* » sera une chaîne de caractères qui ne sera pas condisérée durant l'analyse.

Attention cependant à ne pas avoir deux `quote` dans le même paragraphe, car les *quotes* sont remplacées par un texte en français, vous risquez alors de voir apparaître une erreur de redondance de mots.
