---
date: 2022-10-29T19:03:06+02:00
title: files/
weight: 30
---

Utilisez ce dossier pour y placer les fichiers que vous mettez à disposition des participants.

{{% notice warning %}}
Les fichiers binaires, avec lesquels on ne peut pas faire de diff, n'ont pas d'intérêt à être présents directement sur le dépôt : cela consomme inutilement de la place et rend le clonage du dépôt inutilement long. Vous **devez** utiliser [git-lfs](https://docs.gitlab.com/ee/topics/git/lfs/) sur le GitLab du CRI pour tous les fichiers binaires contenus dans ce dossier.
{{% /notice %}}

Chaque fichier doit avoir une entrée correspondante dans le fichier [`DIGESTS.txt`](#DIGESTS.txt).

- Pas plus  4GB à télécharger **par étape** (ie. tous les fichiers de cette étape) : 4GB, c'est jusqu'à 5 minutes de téléchargement, c'est vraiment beaucoup lorsqu'on est impatient de faire l'étape.

- Archives `.tar.bz2`, `.tar.gz`, `.tar.xz` ou `.zip` lorsque nécessaire. **PAS** de `.rar`, ...

- Compresser (sans tarball, juste via **gzip**[^gz]) les fichiers lorsque c'est utile (memory dump, images BMP, disques, fichiers textes, ...).

  Indiquez dans le fichier `DIGESTS.txt` le hash du fichier compressé (utilisé par la plateforme pour vérifier que le fichier distribué n'a pas été altéré) **et** le hash du fichier initial décompressé (pour l'affichage sur la plateforme).

- Utiliser `$(split -b 240M -d BIG_FILE BIG_FILE.)` pour uploader les gros fichiers (en cas d'erreur 413 notamment).

  Ces fichiers seront concaténés automatiquement au moment de leur import sur la plateforme.

  Seul le hash du fichier entier est requis dans le fichier `DIGESTS.txt`.

[^gz]: l'intérêt de `gzip` est que le serveur web sera capable de distribuer le fichier sans faire apparaître la compression. Voir le [module nginx utilisé](https://nginx.org/en/docs/http/ngx_http_gzip_static_module.html).

## `DIGESTS.txt`

Le fichier `DIGESTS.txt` se trouve dans le répertoire `files/` de chaque étape. Il contient les condensats des fichiers se trouvant dans le dossier respectif.

On le génère avec la commande suivante :

```sh
b2sum * > DIGESTS.txt
```

{{% notice warning %}}
Ce fichier est à générer **avant** l'upload. Son utilité est d'avoir un moyen
de vérifier, une fois sur place, sans connexion Internet, que l'intégralité de
l'arborescence n'a pas été altérée et que les fichiers servis sont bien les
mêmes que sur vos dépôts Git.
{{% /notice %}}

La commande `b2sum` fait partie des *GNU Core Utilities* depuis la [version 8.26](https://github.com/coreutils/coreutils/commit/ea94589e9ef02624a3837f97f80efd7d3dcf56bf). Pour Windows, le site officiel du projet fourni [une archive avec un binaire utilisable](https://www.blake2.net/#su).

{{% notice info %}}
L'algorithme [blake2b](https://blake2.net/) est utilisé à la place d'un SHA-1 ou MD5 car il est plus rapide que ces derniers et est encore considéré comme sûr.
{{% /notice %}}


### Cas des fichiers en plusieurs parties

Dans le cas où vous êtes contraints de découper vos fichiers avant de les uploader, seule la somme de contrôle du fichier entier, avant découpage, est nécessaire.


### Cas des fichiers compressés (`gzip`és)

Si vous avez `gzip`é votre fichier pour qu'il soit distribué décompressé, indiquez dans votre `DIGESTS.txt` à la fois :

* **le condensat du fichier compressé :** il sera utilisé par la plateforme lors de l'import de vos étapes afin de s'assurer que les fichiers n'ont pas été altéré durant l'un des multiples transferts,
* **le condensat du fichier initial, décompressé :** c'est celui qui sera affiché dans l'interface, aux participants.


### Exemple

```
3222734c6c8782682a9c36135a3518e8f4d1facabf76e702cf50da0037a4ed0a425e51266c2914fb83828573e397f96c2a95d419bd85919055479d028f51dba5  fic2016.jpg
023939b0c52b0dfce66954318ab82f7a8c10af4c79c8d5781612b58c74f3ace056067d7b15967e612b176a186b46d3d900c4db8881ba47202521eec33e5bb87b  fic.org
7c91450239cf9b0717642c55c3429dd7326db26e87d4ca198758053333f0640ee89d2dd9b2f1919598f89644b06aa8fc2085648e3d1e542a6db324c9b16a0bdf  header.tex
```


## Distribuer un fichier comme un indice

Dans certaines situations, un fichier peut être optionnel pour valider le défi, mais celui-ci peut donner un avantage certain à l'équipe qui le récupère.
Par exemple, fournir le profile Volatility d'un Linux peut faire gagner beaucoup de temps, ou encore, lorsque l'on n'est pas en mesure de déchiffrer un binaire à analyser, on peut envisager de fournir le binaire déchiffré ou dépacké, ... Mais pas automatiquement !

Dans ce cas on parle de fichiers « indices », c'est-à-dire qu'ils seront distribués comme des indices et pourront coûter un certain nombre de points aux équipes qui les téléchargent.

Dans le fichier `challenge.toml`, il vous faudra y faire référence dans une section `[[hint]]` :

```toml
[[hint]]
filename = "foobar.exe"
cost = 10
title = "Binaire non packé"
```


## Cacher un fichier tant qu'un flag n'a pas été validé

C'est au(x) flag(s) d'indiquer quel(s) fichier(s) ils verrouillent tant qu'ils n'ont pas été validé par un participant.

Dans le fichier `challenge.toml`, pour un `[[flag]]` donné, on ajoutera :

```toml
[[flag]]
...

  [[flag.unlock_file]]
  filename = "foobar.csv"
```

Ici, le fichier `foobar.csv` sera débloqué dès lors que le flag auquel il est rattaché est validé.

Il est possible de faire en sorte qu'un fichier ne soit débloqué qu'après la validation de plusieurs flags, en ajoutant le même `[[flag.unlock_file]]` au niveau des autres flags à débloquer.


## Ne pas distribuer un fichier avant l'archivage du site

Si votre défi dépend de ressources en ligne, qui peuvent ne pas être accessibles le jour du challenge, ou qui ne seront plus accessibles une fois la compétition passée, vous devez inclure une archive avec ces ressources. Il peut s'agir de captures d'écran, d'une version aspirée du site, ...

Il faudra alors faire figurer dans [le `challenge.toml`]({{% relref "challenge.md" %}}#les-fichiers) une référence au fichier, avec un attribut `hidden` :

``` toml
[[file]]
filename = 'rnicrosoft.io.tar.xz'
hidden = true
```

L'archive du site rnicrosoft.io ne sera dévoilée que sur la version archivée du site ou pendant la compétition par une intervention manuelle de l'équipe serveur, si jamais il y avait un soucis de connectivité.


## Récupérer un fichier depuis une autre source que le dépôt

Dans certaines conditions, il peut être pratique de stocker un fichier en ligne, de récupérer un fichier en ligne, plutôt que de l'inclure dans le dépôt du projet (notamment pour les gros dump mémoire/disque).

{{% notice warning %}}

Cette méthode n'est pas recommandée car elle ne permet pas d'avoir un historique complet. De plus, le fichier en ligne peut se trouver modifier sans que le dépôt soit mis à jour (le `DIGESTS.txt` doit être changé en parallèle), ce qui peut conduire un exercice à ne plus être importable à un moment, alors qu'il l'était avant. Néanmoins pour les gros fichiers, afin de rester dans les quotas imposés sur GitLab, c'est une alternative intéressante.

{{% /notice %}}

``` toml
[[file]]
filename = 'my_memory.dump'
url = 'https://drive.srs.epita.fr/aBcDeF0123456/'
```

Il est également nécessaire d'ajouter une entrée dans le fichier `DIGESTS.txt`, de la même manière que pour un fichier du dépôt.


## D'autres attributs ?

Les fichiers peuvent posséder des attributs spécifiques dans [le `challenge.toml`]({{% relref "challenge.md" %}}#les-fichiers).

## Rendu

![Rendu fichiers](exercice_files.png)
