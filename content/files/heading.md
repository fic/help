---
date: 2022-10-29T18:40:39+02:00
title: heading.jpg
weight: 15
---

Il s'agit d'une image/photo représentative de votre scénario/exercice.
Elle est affichée sur la page d'accueil dans une boîte montrant votre scénario/exercice parmi les autres ; et elle est aussi affichée dans le fond de la page lorsque l'on consulte les pages de votre scénario/exercice.

{{% notice info %}}
Attention aux licences et contraintes (notamment l'obligation de citer la source ou le photographe). Celles-ci ne sont pas optionnelles étant donné que l'on veut diffuser nos challenges au public.

Vous devriez utiliser [Unsplash](https://unsplash.com/), partager un travail personnel ou bien encore profiter d'[une intelligence artificielle](https://labs.openai.com/).
{{% /notice %}}

La taille et l'orientation de l'image n'a pas d'importance, mais gardez en tête que c'est son centre qui sera affiché, si la hauteur dépasse la taille prévue.


### Rendu

![Rendu heading.jpg](scenario_heading.png)
