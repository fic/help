---
date: 2019-04-04T15:59:52+02:00
title: links.txt
weight: 35
---

+ `links.txt` : webographie publiée avec les solutions
    - un lien par ligne
    - format d'une ligne : `https://lien Description`
      le premier ' ' est utilisé comme séparateur entre le lien et sa description
    - liens vers les CVE concernées, metasploit/exploitDB, article qui vous a aidé, extrait/dépôt de code, ...


## Exemple

```
https://media.ccc.de/... Vidéo d'inspiration
https://metasplo.it/ Exploit utilisé
https://nist.gov/ CVE-2016-4242
```


## Rendu

{{% notice info %}}
Le contenu du fichier `links.txt` n'est pas encore rendu sur la plate-forme.
{{% /notice %}}
