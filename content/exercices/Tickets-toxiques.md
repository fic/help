---
date: 2024-04-25T13:34:21+01:00
title: Tickets toxiques
---

En pleine période de tension pour les hôpitaux, un prestataire spécialisé en systèmes informatiques hospitaliers se trouve face à un sérieux casse-tête : un déni de service frappe son système de gestion de tickets GLPI, menaçant le fonctionnement critique du support client.

{{% tag %}}Escalade de privilèges{{% /tag %}}
{{% tag %}}DoS{{% /tag %}}

Contexte du défi
----------------

Dans le cadre d’un support client, un prestataire spécialisé dans les SI hospitaliers expose un GLPI pour la gestion des tickets et du parc. Un déni de service est alors remarqué pendant une période de tension pour les hôpitaux et le prestataire.


Pas-à-pas
---------

Vous utiliserez la CVE CVE-2022-35914 et CVE-2022-35947 pour provoquer un déni de service sur le GLPI mais également déposer fichiers malveillants pour permettre de maintenir un accès au système compromis.


Traces à enregistrer
--------------------

- voir [CERTFR-2022-ALE-010](https://www.cert.ssi.gouv.fr/alerte/CERTFR-2022-ALE-010/)
