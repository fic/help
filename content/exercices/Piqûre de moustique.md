---
date: 2023-04-11T23:00:00+01:00
title: Piqûre de moustique
---

Les injections dans des processus, que ce soit sur un système Windows, Linux ou Mac, sont des sujets très peu vus à l’école. Quoi de mieux pour découvrir cette thématique que de réaliser un exercice à ce sujet ?

Cet exercice consiste à découvrir une injection sous Linux (utilisation de la variable d’environnement LD_PRELOAD…), ainsi qu’à découvrir le format ELF.

{{% tag %}}Linux{{% /tag %}}

{{% tag %}}Système{{% /tag %}}

{{% tag %}}ELF{{% /tag %}}

{{% tag %}}Injection{{% /tag %}}

{{% tag %}}Moyen{{% /tag %}}

## Contexte

Anne Pano’Nyme est infirmière dans un hôpital de province. Depuis quelques années, elle s’est prise de passion pour l’informatique et s’y exerce dans son temps libre. Elle s’est essayée à divers domaines et a même récemment mis en place un serveur chez elle !

Cependant, depuis quelques jours, son serveur semble ramer. Anne se souvient que peu de temps avant, elle a installé un programme envoyé par un collègue. Se pourrait-il que cet exécutable eut été malveillant ?

Forte de ses compétences acquises au fil des ans, Anne débute son investigation.



## Infrastructure à mettre en place

Seront nécessaires dans l'infrastructure :
- 1 client/VM (Linux)

## Pas-à-pas

Etudier les différentes méthodes d’injection sous Linux
Etudier le format ELF

## Traces à enregistrer

Fichiers à fournir :
- un dump de RAM du PC

Questions à poser :
- Nom du programme malveillant (on le trouve dans le dump de RAM)
- Condensat de l’exécutable récupéré

## Liens
