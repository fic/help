---
date: 2023-04-08
title: Chiffrement Maison !
---

Analyse d'un ransomware basique avec une crypto maison vulnérable.

Le but est vraiment d'orienter l'exercice sur l'analyse de la crypto.

{{% tag %}}Crypto{{% /tag %}}

{{% tag %}}Reverse{{% /tag %}}

{{% tag %}}Difficile{{% /tag %}}



Contexte du défi
----------------

Un cybergang à lancé sont dernier ransomware à la mode ! Il a malheureusement chiffré plusieurs ordinateurs sensible !

Pourrait-on récupérer ces données ?


Infrastructure à mettre en place
--------------------------------

- N/A


Pas-à-pas
---------

- analyse du binaire malveillant

- compréhension du mécanisme crypto

- exploiter une vulnérabilité pour arriver à inverser le chiffrement

- déchiffrement d'un fichier donné


Risques
-------

- la crypto doit être suffisament complexe pour nécessiter un peu de travail du joueur

- Suivant la vulnérabilité choisit, ça peut par exemple nécessiter d'avoir X fichier chiffré pour pouvoir reverse la crypto ?

- bonne compréhension des mécanisme crypto peut être utile

- la complexité doit être au niveau crypto, pas du reverse, donc il faudrait faire un truc vraiment simple à ce niveau là (pas de rust par exemple), ça pourrait même être un executable python packagé.


Traces à enregistrer
--------------------

- le malware chiffrant

- un fichier chiffré à déchiffré


Liens
-----

