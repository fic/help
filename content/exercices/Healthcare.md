---
date: 2024-04-25T13:34:21+01:00
title: Healthcare
---

Face à la surprenante découverte d'un produit concurrent étrangement similaire lors d'un salon technologique, la société Healthcare, pionnière en détection par imagerie médicale, se trouve contrainte d'initier un audit de sécurité rigoureux pour évaluer l'intégrité et la confidentialité de ses développements en recherche et développement.

{{% tag %}}Social engineering{{% /tag %}}

Contexte du défi
----------------

Lors d’un salon technologique, la société Healthcare – startup pioniere dans la détection par imagerie médicale - découvre qu’un concurrent présente au salon un produit étrangement similaire a un de leurs produit en R&D.

Afin de s’assurer qu’aucun vol de données n’est été réalisée au sein de leur entreprise, la société mandate un audite de sécurité.


Infrastructure à mettre en place
--------------------------------

Aidez les étudiants à identifier le travail préparatoire à faire pour mettre en place cet exercice.

Lorsque l'exercice se déroule sur un réseau, il convient de leur donner un schéma plutôt qu'une longue explication.


Risques
-------

Si votre exercice n'est pas clef en main, quels éléments doivent encore être précisés/trouvés avant qu'il ne soit possible de commencer la réalisation de l'exercice (par exemple l'attaque initiale peut être laissé à l'appréciation des étudiants, ou bien peut-être faut-il trouver le logiciel vulnérable qui va bien, ...).


Pas-à-pas
---------

A l’aidede social engineering, un ingénieur de la société Healthcare a été
pris pour cible.

1. Une pièce jointe envoyé par l’attaquant afin d’avoir un reverse-shell sur le PC de la victime.
1. Vous utiliserez la CVE-2023-7028 afin de réinitialiser le mot de passe du GitLab interne. (Le gitlab n’estpas exposer sur internet).
1. Vous exfiltrer le contenu du gitlab via la boite mail.


Traces à enregistrer
--------------------

- Boite mail avec mail de phishing ciblé et piece jointe
- Trafic réseau
- `gitlab-rails/production_json.log`
- `gitlab-rails/production_json.log`
