---
date: 2023-04-08
title: GeoGuessr niveau expert
---

Retrouver un lieu précis à partir d'un témoignage précis (liste d'indication) (avec l'utilisation très recommandée d'un outil de type overpass turbo )

{{% tag %}}osint{{% /tag %}}
{{% tag %}}facile{{% /tag %}}


Contexte du défi
----------------

Vous faites partie d'une cellule d'enquête chargée de retrouver le lieu ou ont été cachée des pièces à convictions, vous n'avez que des déclarations de lieu et de distance relative. Arriverez vous à trouver l'endroit ?


Infrastructure à mettre en place
--------------------------------

N/A


Pas-à-pas
---------

Soyez plus ou moins original.


Risques
-------

Trouver un lieu trouvable soit comme un bourrin sur google maps (+++ long) soit en passant par overpass turbo (mais il faut que le lieu ou en tout cas les élements menant au lieu, soient référencées sur openstreet map vu que c'est ce sur quoi se base le tool)


Traces à enregistrer
--------------------

- Description des lieux et du chemin


Liens
-----

[Overpass Turbo](https://www.bellingcat.com/resources/2020/12/03/using-the-sun-and-the-shadows-for-geolocation/
https://overpass-turbo.eu/)

[ex d'utilisation d'overpass turbo](https://github.com/BreizhCTF/breizhctf-2023/blob/main/challenges/osint/bar_ou_turbot/WriteUp/WriteUp.md)