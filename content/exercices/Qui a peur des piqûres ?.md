---
date: 2023-04-10T23:00:00+01:00
title: Qui a peur des piqûres ?
---

Les injections dans des processus, que ce soit sur un système Windows, Linux ou Mac, sont des sujets très peu vus à l’école. Quoi de mieux pour découvrir cette thématique que de réaliser un exercice à ce sujet ?

Cet exercice consiste à découvrir l’injection appelée « Process Hollowing », sous Windows, ainsi qu’à découvrir le format PE.

{{% tag %}}Windows{{% /tag %}}
{{% tag %}}Système{{% /tag %}}
{{% tag %}}PE{{% /tag %}}
{{% tag %}}Injection{{% /tag %}}
{{% tag %}}Moyen{{% /tag %}}

Contexte du défi
----------------

Anne O’Nyme est infirmière dans un hôpital de province. Depuis quelques années, elle s’est prise de passion pour l’informatique et s’y exerce dans son temps libre. Elle s’est essayée à divers domaines et a même récemment mis en place un serveur chez elle !

Cependant, depuis quelques jours, son serveur semble ramer. Anne se souvient que peu de temps avant, elle a installé un programme envoyé par un collègue. Se pourrait-il que cet exécutable eut été malveillant ?

Forte de ses compétences acquises au fil des ans, Anne débute son investigation.

Infrastructure à mettre en place
--------------------------------

Seront nécessaires dans l'infrastructure :
1 serveur/VM (Windows Server)

Pas-à-pas
---------

L’exécutable fourni par le collègue d’Anne était en fait un logiciel malveillant (cryptominer, …) !

En effet, une fois exécuté, celui-ci s’injecte dans un processus Windows à l’aide du Process Hollowing, rajoute quelques points de persistance et supprime ensuite le fichier d’origine.

En tant que joueur, les étapes de résolution sont les suivantes :
- Déterminer le processus malveillant dans le dump de RAM
- Examiner la mémoire de ce processus et en récupérer le PE du binaire
- Analyser le binaire récupéré pour répondre aux questions

Traces à enregistrer
--------------------

Fichiers à fournir :
- un dump de RAM du serveur d’Anne

Questions à poser :
- Nom du programme malveillant (on le trouve dans le dump de RAM)
- Nom du processus « malveillant » (car sans l’injection il n’est pas malveillant)
- Condensat de l’exécutable récupéré

Liste des fonctionnalités du binaire : - quelques éléments de persistances, but du binaire (cryptominer ou autre), auto-suppression, …

Ressources
-----

[La documentation Microsoft sur les PE](https://learn.microsoft.com/en-us/windows/win32/debug/pe-format)

[Process Hollowing](https://attack.mitre.org/techniques/T1055/012/)
