---
date: 2023-03-24T13:34:21+01:00
title: Trampoline
---

Vous allez modifier un antivirus pour laisser passer votre propre cheval de Troie !
C’est le moment de bidouiller ClamAV.

Ou vous pouvez exploiter un antivirus, à vous de voir. 

{{% tag %}}Antivirus{{% /tag %}}

Contexte du défi
----------------

Un PC est compromis par un ransomware.
Pourtant, l’antivirus est censé bloquer ce malware sorti il y a déjà quelques temps… Vous investiguez

Infrastructure à mettre en place
--------------------------------

Libre.
Quelques exemples : 
- Un routeur sous openBSD
- Un serveur de mail
- Un poste d’un particulier

Pas-à-pas
---------

Vous devez modifier un antivirus (opensource) ou compromettre un antivirus déjà en place.

L’antivirus modifié ou compromis laisse passer le malware.

Risques
-------

Trouver une compromission d’antivirus est plus difficile que de modifier un antivirus open source. Il faut aussi trouver la bonne version de l’antivirus

Dans le cas d’une modification d’un antivirus, il faut lire un minimum de code pour comprendre vaguement son fonctionnement. 

Traces à enregistrer
--------------------

Fichiers à fournir :
- Dump de RAM
- Dump Filesystem 
- Logs système (dont antivirus)

Questions à poser :
- Implementation de l’antivrus
- Pourquoi le malware est passé 
- Impact de l’antivirus sur le système

Liens
-----

[Clamav open-source](antivirus (https://docs.clamav.net/manual/Installing/Installing-from-source-Unix.html)

[Windows CVE-2021-31985](https://www.pixiepointsecurity.com/blog/nday-cve-2021-31985.html) (Si vous arrivez à le faire fonctionner…)