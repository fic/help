---
date: 2024-04-04
title: Packing vers l'infini
---

Un malware peut souvent utiliser du packing pour se dissimuler. Ils existe des packer connu ainsi que des solutions pour "unpack" ces malwares. L'unpacking peut aussi se faire à la main (analyse dynamique en essaynt par exemple de break au moment ou le binaire est dechiffré).

Il pourrait être marrant de faire son propre système de packing, puis de faire en sorte que le malware se pack par exemple 1000 fois? 5000 fois ?. Le flag se trouvant dans la version finale. Cela obligerait le joueur a automatiser l'unpacking. 




{{% tag %}}Reverse{{% /tag %}}

{{% tag %}}Malware{{% /tag %}}

{{% tag %}}Moyen{{% /tag %}}

Contexte du défi
----------------

Un malware a reverse

Infrastructure à mettre en place
--------------------------------


Pas-à-pas
---------

- réaliser un packer custom (si possible pas non plus trop complexe)
- faire en sorte de pouvoir pack N fois le malware
- cacher le flag dans le binaire final packer de très très nombreuse fois.


Risques
-------

- Ne pas utiliser de packer connu et avec un système d'unpack déjà automatisé
- faire un packer trop complexe
- Ne pas avoir un binaire final gigantesque (après on peut se supposer que ça peut être ok d'avoir un binaire de plusieurs dizaines de mega voir plus suivant comment votre packing se fait)


Traces à enregistrer
--------------------

Fichiers à fournir :
- le malware packé X fois

Questions à poser :

- flag final
- question sur le packer ? 


Liens
-----

