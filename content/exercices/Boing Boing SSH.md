---
date: 2023-03-24T13:34:21+01:00
title: Boing Boing SSH
---

L'usage d'un agent SSH et des fonctionnalités de *forwarding* sont très puissants, mais peuvent se révéler désastreux dans le cadre d'une infrastructure de production pas toujours au fait de l'état de l'art.

À partir d'une application vulnérable, cet exercice consiste à se latéraliser sur d'autres serveurs de production, en profitant de l'agent SSH transmis par un développeur pour `pull` un dépôt.

{{% tag %}}Infra{{% /tag %}}
{{% tag %}}Linux{{% /tag %}}
{{% tag %}}SSH{{% /tag %}}
{{% tag %}}Moyen{{% /tag %}}

Contexte du défi
----------------

C'est la panique chez TechTon'Eak, après avoir fait face la semaine dernière au défacement de leur site principal par le groupe d'hacktivistes TechWave. Aujourd'hui tout leur système d'information tourne au ralenti : la quasi-totalité des serveurs de production exécutent un cryptominer.

Peut-être reste-il des indices dans les journaux du système ?


Infrastructure à mettre en place
--------------------------------

Seront nécessaires dans l'infrastructure :
  - 1 routeur (non concerné par les traces, c'est « l'hébergeur » qui le gère) ;
  - 3 serveurs/VM (sous Rocky Linux par exemple) ;
  - 2 postes clients (peu importe le système, non concernés par les traces).

Les serveurs utilisés :
  - 1 serveur web/vitrine/PHP (exposé sur internet)
  - 1 serveur web interne (CRM, facturation, Point Of Sales, ...)
  - 1 serveur base de données (utilisé par les deux serveurs)

Les postes clients (hors du réseau des serveurs) :
  - 1 poste pour l'attaquant
  - 1 poste pour le développeur/admin sys prestataire qui va redéployer le site


Pas-à-pas
---------

Le défacement de la vitrine a aussi été l'occasion pour l'attaquant de s'infiltrer sur le système (webshell, ...).

À cette occasion, l'attaquant en profite pour faire une reconnaissance rapide (on doit voir dans les logs des tentatives de connexion SSH sur les autres serveurs).

Lorsqu'un développeur est chargé de remettre le site en état, il se connecte sur le serveur (avec `ssh -A` ou similaire). Il a besoin de l'agent SSH pour récupérer les sources du site sur GitHub/GitLab/...

Au moment de la connexion du développeur, l'attaquant en profite pour utiliser l'agent rendu accessible pour se connecter aux autres serveurs. Il lance alors un `curl | sh` qui télécharge et installe un cryptominer.


Risques
-------

L'attaque initiale (le défacement) doit s'effectuer au travers de la vitrine exposée sur Internet. Ça peut être Wordpress ou un autre CMS suffisamment récent et disposant d'une vulnérabilité à exploiter.

La vitrine doit être un minimum convainquante (n'hésitez pas à faire générer du contenu cohérent à des IA pour remplir et illustrer le site).

Le CRM doit être rempli, avec des factures convainquantes.

Voir comment l'attaquant obtient l'accès à l'agent SSH : stocké dans `/tmp`/`/run`/... (incompatible avec `systemd`), besoin de passer `root` ou bien le développeur se connecte en tant qu'utilisateur `www-data`, ...


Traces à enregistrer
--------------------

Fichiers à fournir :
- un dump de RAM de la machine hébergeant la vitrine
- logs de la machine (au format brut `journalctl`)
- `auth.log` des 3 serveurs du parc

Questions à poser :
- Nom du programme malveillant (on le trouve dans le dump de RAM)
- Adresse du porte-feuille bitcoin (on extrait rapidement le binaire du dump de RAM, l'adresse peut apparaître en faisant un `strings`)
- Heure à laquelle les connexions suspectes ont lieu (on constate dans les `auth.log` des machines des heures de connexion très proches, qui doivent faire penser à un script)
- Protocole utilisé par les connexions suspectes (dans les logs ont voit que c'est lié à SSH)
- Emplacement du fichier ayant conduit à la compromission (on attend la socket de l'agent SSH)
- Type de fichier (agent SSH)

Autres questions possibles :
- Liste des éléments de persistances (crontab, timers, service systemd, ...) mis en place par le `curl | sh`.
- Option du serveur à changer


Liens
-----

* [Post-mortem d'une attaque incluant l'usage d'SSH Agent Forwarding](https://matrix.org/blog/2019/05/08/post-mortem-and-remediations-for-apr-11-security-incident)
* [Using SSH agent forwarding](https://docs.github.com/en/authentication/connecting-to-github-with-ssh/using-ssh-agent-forwarding)
