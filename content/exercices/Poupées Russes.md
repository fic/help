---
date: 2023-04-11T23:00:00+01:00
title: Poupées Russes
---

Êtes-vous sûrs que le PDF que vous venez d’ouvrir avec votre logiciel de visualisation préféré ne pourrait pas en fait être aussi une page HTML ?

Certains formats de fichiers peuvent se faire passer pour d’autres types de fichiers. On les appelle les « fichiers polyglottes ».

Travailler sur ces fichiers permet de vous faire découvrir un sujet fun tout en permettant aux joueurs de tenter de résoudre un exercice original.

{{% tag %}}Formats de fichiers{{% /tag %}}
{{% tag %}}Difficile{{% /tag %}}

## Contexte du défi

Libre.

## Infrastructure à mettre en place

Seront nécessaires dans l'infrastructure :
- 1 client/VM

## Pas-à-pas

Déterminer les formats de fichiers qu’il est possible d’utiliser
Tester les générateurs de fichiers polyglottes.

Soyez créatifs ! Faites-en sorte de ne pas juste changer l’extension du fichier pour résoudre l’exercice. Par exemple, votre fichier pourrait être une image contenant une indication permettant de déchiffrer l’archive, lorsque le fichier est mis au format .zip

## Traces à enregistrer

Fichiers à fournir :
- à déterminer

Questions à poser :
- Liste des formats par lesquels il faut passer pour atteindre l’exécutable
- Condensat de l’exécutable récupéré

## Ressources

[Le GitHub d’Ange Albertini](https://github.com/angea)
