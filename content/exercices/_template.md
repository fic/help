---
date: 2023-03-24T13:34:21+01:00
title: Titre du défi
# draft est employé pour ne pas publier la page _template.md, à retirer
draft: true
---

Introduction brève et résumé.

{{% tag %}}Thème principal{{% /tag %}}
{{% tag %}}Thème secondaire{{% /tag %}}
{{% tag %}}Difficulté (pour les étudiants){{% /tag %}}

Contexte du défi
----------------

Ici il s'agit de l'énoncé tel que les participants le verront.

Il faut donner suffisamment de contexte et des objectifs clairs pour se lancer facilement dans l'exercice.


Infrastructure à mettre en place
--------------------------------

Aidez les étudiants à identifier le travail préparatoire à faire pour mettre en place cet exercice.

Lorsque l'exercice se déroule sur un réseau, il convient de leur donner un schéma plutôt qu'une longue explication.


Risques
-------

Si votre exercice n'est pas clef en main, quels éléments doivent encore être précisés/trouvés avant qu'il ne soit possible de commencer la réalisation de l'exercice (par exemple l'attaque initiale peut être laissé à l'appréciation des étudiants, ou bien peut-être faut-il trouver le logiciel vulnérable qui va bien, ...).


Pas-à-pas
---------

Ici ça peut être l'occasion de donner un peu de contexte aux étudiants pour les aider à comprendre avec un mini-scénario.
Ou bien les étapes à suivre pour réussir l'exercice.


Traces à enregistrer
--------------------

Décrivez ici les fichiers qui devront être générés par les étudiants.

Lorsque vous avez des propositions de flags, indiquez-les ici.


Liens
-----

* [Sources](https://superblog.com)
* [inspirations](https://superblog.com)
* [...](https://superblog.com)
