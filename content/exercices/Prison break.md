---
date: 2023-03-24T13:34:21+01:00
title: Prison break
---

{{% tag %}}Apple{{% /tag %}}
{{% tag %}}Bypass{{% /tag %}}

Contexte du défi
----------------

Les informations confidentielles de votre entreprise ont fuité. La seule origine possible est une machine sous macOS. 

Infrastructure à mettre en place
--------------------------------

Une machine sous macOS.

Pas-à-pas
---------

Vous devez créer un environnement sur une machine macOS aillant activé le SIP (System Integrity Protection).

Le SIP doit être contourné par un binaire malveillant.

Risques
-------

Vous devez avoir le matériel (mac).  

Le matériel doit être sous la bonne version.

Votre système doit utiliser SIP.

Traces à enregistrer
--------------------

Fichiers à fournir :
- logs système propres aux mac
- dump de RAM ou système de fichiers

Questions à poser :
- Spécificités qui montrent que SIP a été contourné  

Liens
-----

[SIP](https://support.apple.com/en-us/HT204899)

[Analyse de la CVE 2022-22583](https://perception-point.io/blog/technical-analysis-cve-2022-22583/)

[PoC CVE 2022-22583](https://github.com/fzxcp3/CVE-2022-22583/tree/main/CVE-2022-22583)

[Ressources sur la sécurité d’Apple](https://help.apple.com/pdf/security/en_GB/apple-platform-security-guide-b.pdf)