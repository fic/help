---
date: 2024-04-24T13:34:21+01:00
title: Unlimited power !  
---

Les composants noyaux s'exécutant sur un système d'exploitation permettent une liberté d'action quasiment totale sur l'ensemble du système.

À partir d'une vulnérabilité d'un composant noyau légitime, un attaquant compromet une machine critique au sein d’une entreprise ou d’un particulier.

Vous créez ou utilisez un driver noyau vulnérable comme point de départ.

Ce Petit-Exercice peut être utilisé par plusieurs groupes facilement en changeant l'OS du composant noyau.

{{% tag %}}Bas niveau - Noyau{{% /tag %}}

{{% tag %}}Driver{{% /tag %}}

{{% tag %}}Moyennement difficile{{% /tag %}}

Contexte du défi
----------------

L’utilisateur remarque que son PC ne fonctionne pas très bien. Il lui arrive d'avoir des erreurs impossibles à corriger (BSOD, Grey screen, kernel paninc, ...). Vous investiguez.


Infrastructure à mettre en place
--------------------------------

Il faut un PC capable de charger un driver noyau

Pas-à-pas
---------

Vous devez créer ou utiliser un driver noyau vulnérable linux/windows/macos.   
Le driver présente une vulnérabilité permettant de faire des actions malveillantes et impacte le système du joueur.   
Le joueur doit reconnaître les impacts, comprendre les actions, reverse le driver.  
En reversant le driver il explique comment les actions ont eu lieu.

Risques
-------

La mise en place de l’exercice peut être compliquée.   
Il faut développer ou modifier un driver Windows/Linux ou MacOS ou utiliser un vieux driver déjà vulnérable.
Exercice bas-niveau, il faut se documenter pour comprendre le fonctionnement de l’environnement de votre OS favori.

Traces à enregistrer
--------------------

Fichiers à fournir :
- un dump de RAM de la machine cible
- des logs pertinents de l’attaque

Questions à poser :
- Questions sur le code de votre driver
- Questions sur les impacts du driver malveillant 


Liens
-----

* [Ressources sur le développement noyau Windows](https://learn.microsoft.com/en-us/windows-hardware/drivers/gettingstarted/)
* [Ressources sur le développement noyau MacOS](https://developer.apple.com/documentation/kernel)
* [Ressources sur le développement noyau Linux](https://lwn.net/Kernel/LDD3/)