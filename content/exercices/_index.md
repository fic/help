---
date: 2023-03-24T13:26:20+01:00
title: Idées de défis
weight: 8
---

Vous trouverez dans cette section des propositions d'exercices individuels.

Durant votre 1er semestre de SRS, vous allez concevoir des défis selon une thématique qui vous intéresse. Vous pouvez utiliser directement une proposition d'exercice pour la concrétiser (penser à la réserver pour ne pas qu'une autre équipe fasse le même exercice que vous).

Lorsque vous serez plus à l'aise avec les attentes du challenge, nous vous encourageons à imaginer vos propres défis et à nous les soumettre en soutenance.

{{% children %}}
