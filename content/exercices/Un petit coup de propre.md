---
date: 2023-04-10T23:00:00+01:00
title: Un petit coup de propre
---

Après qu’un attaquant ait réussi à compromettre une machine, il cherche à s’y pérenniser.

Cet exercice consiste à débusquer les différents points de persistance mis en place afin que l’attaquant puisse avoir accès en toutes circonstances à cette machine.

{{% tag %}}Windows{{% /tag %}}

{{% tag %}}Système{{% /tag %}}

{{% tag %}}Persistance{{% /tag %}}

{{% tag %}}Moyen{{% /tag %}}

Contexte du défi
----------------

Vous venez d’être embauché dans la boîte de sécurité informatique très tendance : Hack’N’Snack.
Histoire de tester vos connaissances système (parce que vous avez dit être « le boss » sur le sujet), on vous demande de retrouver le logiciel malveillant caché sur un PC sous Windows et de le nettoyer.

Vous êtes motivé pour faire vos preuves. Alors attention aux yeux !

Infrastructure à mettre en place
--------------------------------

Seront nécessaires dans l'infrastructure :
- 1 client/VM (Windows 11)

Pas-à-pas
---------

Etudier les différentes méthodes de persistance existantes

En imaginer quelques unes

Traces à enregistrer
--------------------

Fichiers à fournir :
- un dump de RAM du PC
- un dump de disque du PC

Questions à poser :
- Nom du programme malveillant (on le trouve dans le dump de RAM)
- Condensat de l’exécutable récupéré
- Liste justifiée des éléments de persistances mis en place

Liens
-----

* Practical Malware Analysis, édition NoStarchPress
