---
date: 2023-04-24T23:00:00+01:00
title: Ça tourne !
---

Sur Windows, toutes les exécutions sont tracées par divers moyens. L’un d’eux est appelé « Prefetch ».

L’objectif de l’exercice est de vous faire découvrir ce que sont les Prefetch.

{{% tag %}}Windows{{% /tag %}}
{{% tag %}}Prefetch{{% /tag %}}
{{% tag %}}Moyen{{% /tag %}}

Contexte du défi
----------------

Le SOC d’une PME s’est rendu compte qu’elle s’était faite compromettre. Votre équipe de réponse à incidents a été appelée pour évaluer la situation et préparer la remédiation. Votre première tâche consiste à déterminer si l’attaquant est toujours présent, depuis quand et s’il a vu qu’il avait été repéré.

Infrastructure à mettre en place
--------------------------------

Seront nécessaires dans l'infrastructure :
1 VM (Windows)

Pas-à-pas
---------

Réfléchir aux possibles actions/exécutions de l’attaquant et rendre cela crédible
Faire du bruit pour que la réalisation ne soit pas trop rapide

Traces à enregistrer
--------------------

Fichiers à fournir :
un dump de disque de la VM

Questions à poser :
Date de la première apparition de l’attaquant : il y a un délai de 10 secondes après l’exécution pour que le fichier soit écrit sur le disque (petit piège pour le joueur)
L’attaquant sait-il qu’il a été détecté ? Utilisation d’un wiper suite à sa détection (*sdelete* par exemple)
…

Ressources
-----

Outils d’Eric Zimmermann pour visualiser les prefetchs : PECmd.exe
Outil de Nirsoft WinPrefetchView