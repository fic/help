---
date: 2024-04-24T13:34:21+01:00
title: Passthrough  
---

L'objectif de l'exercice est de modifier openSSH pour qu'il puisse vous laisser entrer dans n'importe quel système sur lequel il serait installé.

{{% tag %}}Compromission{{% /tag %}}

{{% tag %}}OpenSSH{{% /tag %}}

{{% tag %}}Facile{{% /tag %}}

Contexte du défi
----------------

Une entreprise voit ses serveurs déconnectés à un moment inopportun. Elle doute que ce soit un hasard et investigue.


Infrastructure à mettre en place
--------------------------------

1 PC faisant tourner openSSH

Pas-à-pas
---------

Vous devez modifier openSSH.   
L'openSSH modifié doit vous permettre de rentrer sur n'importe quel PC (windows/linux/macos).
Après être rentré sur la cible, vous devez exécuter des actions que le joueur devra comprendre.
Le joueur doit reconnaître les impacts, comprendre les actions de l'openSSH modifié.
  

Risques
-------


Traces à enregistrer
--------------------

Fichiers à fournir :
- un dump de RAM de la machine cible
- des logs pertinents de l’attaque

Questions à poser :
- Questions sur le code de votre openSSH
- Questions sur les impacts
- Les actions entreprises après la connexion SSH


Liens
-----

* [xz-vuln](https://xeiaso.net/notes/2024/xz-vuln/)
