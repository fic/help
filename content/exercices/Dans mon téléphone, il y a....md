---
date: 2023-03-24T13:34:21+01:00
title: Dans mon téléphone, il y a…
---

L’objectif de cet exercice est de travailler sur des formats de fichiers propres à l’écosystème Apple.

{{% tag %}}Téléphone{{% /tag %}}

{{% tag %}}Facile{{% /tag %}}

Contexte du défi
----------------

Vous enquêtez sur la disparition de votre amant.

Vous avez réussi à récupérer son téléphone. Retrouvez-le.

Infrastructure à mettre en place
--------------------------------

Un/des appareils dans l’écosystème Apple.

En fonction du type de fichier que vous voulez faire analyser, il vous faut générer les informations (géolocalisation, images, sons, …).

Pas-à-pas
---------

Votre amant est lui-même informaticien, il sait dissimuler des informations dans les fichiers de ses appareils.

Les fichiers vous aident à comprendre ses itinéraires, rencontres, etc.

Il a dissimulé des informations dans les métadonnées de quelques fichiers.
 
Avec l’ensemble des informations, vous retrouvez sa trace.

Risques
-------

Potentiellement besoin de matériel Apple.   

Traces à enregistrer
--------------------
Fichiers à fournir :
- Ressources acquises sur le téléphone/tablette/Mac ou une sauvegarde

Questions à poser :
- Insister sur les spécificités de l’appareil, la version 
- lieux, endroits
- information dans les métadonnées 

Liens
-----

[Forensic sur iOS](https://www.prplbx.com/resources/blog/ios-forensics-guide/)

[Ressources sur le forensic iOS](https://resources.infosecinstitute.com/topic/ios-forensics/)