---
date: 2024-04-04
title: imprimante malveillante 
---

L'objectif du défi est d'auditer un système de contrôle d'imprimante 3d, afin de trouver les soucis.

L'idée de serait d'avoir un système linux gérant un parc d'imprimante 3d avec pa exemple Octoprint. Ce logiciel de gestion permet d'utiliser des plugins permettant d'influer sur la gestion des impression. On pourrait imaginer qu'une entreprise concurante ai voulu causer des soucis. Plusieurs piste à explorer :

- problème sur l'authentification sur le soft
- logiciel exposé sur internet
- installation d'un plugin backdooré
- modification d'un plugin déjà existant

Les plugins sont en python.

Dans les trucs marrant qui pourrait être à faire:

- modification du GCODE envoyé à l'imprimante (par exemple enelver une couche sur 2 pour rentre que l'impression fasse n'importe quoi, ou modifier un peu les coordonnées généré pour que le resultat soit moche, ou forcer l'impression d'un message de rançon ?)

Le but serait de faire trouver les différents soucis au joueur.


{{% tag %}}Python{{% /tag %}}

{{% tag %}}Audit de code{{% /tag %}}

{{% tag %}}Moyen{{% /tag %}}

Contexte du défi
----------------

Votre super entreprise d'impression 3d est au bord de la faillite ! Toutes les impressions que vous réalisez pour des clients échoue !


Infrastructure à mettre en place
--------------------------------

- Un serveur qui simulera le serveur de gestion (ça peut être un raspberry pi)
- Idéalement un accès à une vrai imprimante 3d pour tester mais sinon il est possible de setup une imprimante virtuelle pour debug (https://docs.octoprint.org/en/master/development/virtual_printer.html). 

Pas-à-pas
---------


Risques
-------

- Si aucun accès à une vrai imprimante il pourra être difficile de valider le resultat final


Traces à enregistrer
--------------------

Fichiers à fournir :

- le file system du server

Questions à poser :

- vulnérabilité(s) ? 


Liens
-----

- https://docs.octoprint.org/en/master/development/virtual_printer.html
- https://octoprint.org/