---
date: 2023-03-24T13:34:21+01:00
title: Oui, allô ?
---

{{% tag %}}Téléphone{{% /tag %}}

Contexte du défi
----------------

Votre ami a passé son téléphone à un inconnu dans la rue.  
Maintenant votre ami s’inquiète.  
Rassurez-le. Ou pas.

Infrastructure à mettre en place
--------------------------------

Un modèle de téléphone sous Android.

En fonction de la vulnérabilité choisi ou si vous utilisez votre logiciel malveillant, il faut la bonne version d’android ou le bon matériel.

Pas-à-pas
---------

Libre du moment que le téléphone est sous Android.

Quelques idées :
- Abuser d’une configuration par défaut ou voulue (dev par exemple)
- ADB 
- Attaques physiques (plug de materiel malveillant, etc.)
- CVE sur un appareil pas à jour
- Sur certains téléphones il est possible, avec un accès physique (le cas de notre contexte) d’accéder ou de déverrouiller des fonctionnalités bas-niveau (bootloader) 

Risques
-------

Besoin de matériel (le téléphone).   
Possibilités de virtualisation du téléphone mais limite les attaques physiques.

Traces à enregistrer
--------------------
Fichiers à fournir :
- Dump de RAM du téléphone
- Dump système de fichier
- Possiblement une APK d’une application

Questions à poser :
- Insister sur les spécificités de l’appareil, la version ou l’implémentation de votre binaire

Liens
-----

[Android Hackers Handbook](https://archive.org/details/AndroidHackersHandbook)