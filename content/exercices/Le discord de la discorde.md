---
date: 2023-04-08
title: Le discord de la discorde
---

Retrouver un coupable d'exfiltrations d'informations par discord.

Idéalement ça pourrait être une copie des fichiers data de discord sur un mobile.

{{% tag %}}Messageries{{% /tag %}}
{{% tag %}}Moyen{{% /tag %}}


Contexte du défi
----------------

Lors de votre enquête vous êtes sur qu'un employé à utilisé une application mobile pour envoyer des documents confidentiels à un concurent. Il ne vous manque plus que la preuve de ces faits pour conclure votre dossier !

Vous avez récupéré une copie des données data de son téléphone. Arriverez vous à trouver quels documents ont été partagés et à qui ?


Infrastructure à mettre en place
--------------------------------

- Téléphone rooté pour récupérer facilement les données discord
- Suffisament d'échange discord pour essayer de noyer le poisson



Pas-à-pas
---------

Libre.


Risques
-------

- une quantité suffisante de message
- discord publique ou non ? (probablement pas le droit)
- les données discord peuvent être pénible à comprendre
- un simple strings / grep ne doivent pas remonter vos infos

Traces à enregistrer
--------------------

- données data du téléphone


Liens
-----

