---
date: 2023-03-24T13:34:21+01:00
title: C’est pas donnée
---

Avec une connexion déjà établie grâce a l’ordinateur de la victime, exfiltrez au plus vite des données de l’entreprise pour les revendre.

{{% tag %}} exfiltration {{% /tag %}}
{{% tag %}} facile {{% /tag %}}


## Contexte du défis
Un employé d’entreprise est parti en vacance avec son ordinateur de travail mais se l’est fait voler.

Ne l’ayant pas remarqué tout de suite, il s’est passé 4 jours avant qu’il prévienne l’entreprise et que ses comptes soient bloqués. 

L’entreprise a peur de ce qui a pu se passer sur ce laps de temps, pouvez vous investiguer ?


## Infrastructure à mettre en place
- Ordinateur de l’utilisateur
- Active Directory de l’entreprise
- Un VPN
- Autres services spécifiques


## Pas-à-pas
Choisir les éléments (applications, serveurs, infrastructure, employés, fichiers) que l’attaquant va pouvoir découvrir grâce a son accès.

Construire une mise en place logique de l’attaque -> Peut-être qu’il regarde déjà ce qui est accessible de l’utilisateur sur lequel il est connecté, peut être qu’il peut déjà commencer a télécharger ses fichiers, qu’est ce qu’il peut chercher par la suite ?

Possibilité de donner les fichiers jour par jour pour séparer les questions.


## Risques
- L’attaquant est pressé, il sait qu’il ne peut pas garder l’accès indéfiniment -> faire retranscrire sa hâte dans les logs ? 
- Si c’est de la recherche il faut montrer des essaies non fructueux, pas seulement l’essaie qui mène au résultat escompté.
- Attention à l’horodatage


## Traces à enregistrer
- Logs de l’AD spécifiques aux actions de l’utilisateur
- Logs réseau

## Liens

