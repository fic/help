---
date: 2023-04-24T23:00:00+01:00
title: La clé du succès
---

L’objectif de cet exercice est de vous faire découvrir les traces laissées par les clés USB.

{{% tag %}}Evtx{{% /tag %}}
{{% tag %}}Registres{{% /tag %}}
{{% tag %}}USB{{% /tag %}}
{{% tag %}}Moyen{{% /tag %}}

Contexte du défi
----------------

Malheur ! La startup Rob’Bot a sorti son nouveau chien robot : c’est le même modèle que celui que votre équipe allait dévoiler deux jours plus tard ! Vous en êtes sûrs, ils ont réussi à vous voler les plans et probablement plus… mais comment ?

Infrastructure à mettre en place
--------------------------------

Seront nécessaires dans l'infrastructure :
1 VM (Windows)

Pas-à-pas
---------

Utiliser RegistryExplorer pour visualiser les registres et trouver les clés contenant les valeurs demandées
Déterminer les logs EVTX (Event Viewer), qui permettent aussi de répondre à certaines questions
Dans les « Documents Récents », il est possible de voir si un fichier a été ouvert sur une clé

Traces à enregistrer
--------------------

Fichiers à fournir :
un dump de disque de la VM

Questions à poser :
Date de première connexion/dernière connexion de la clé USB
Nom et modèle de la clé
Numéro de série
Lettre où elle a été montée lors de son dernier branchement
Quel(s) fichier(s) a/ont été ouvert(s) par inadvertance sur la clé ?
…

Ressources
-----

Outils d’Eric Zimmermann : RegistryExplorer (a un certain nombre de plugins pour aider l’investigateur)
Utiliser l’Event Viewer de Windows pour visualiser les logs