---
date: 2023-03-24T13:34:21+01:00
title: Au commencement 
---

Le BIOS (Basic Input/Output System) et l'UEFI (Unified Extensible Firmware Interface) sont deux types de logiciels de bas niveau qui permettent de démarrer un ordinateur et de contrôler les périphériques matériels tels que le disque dur, le clavier, la souris, etc.

À partir d’un driver UEFI malveillant, un attaquant compromet une machine critique au sein d’une entreprise ou d’un particulier.

{{% tag %}}Bas niveau{{% /tag %}}

{{% tag %}}Driver{{% /tag %}}

{{% tag %}}Très difficile{{% /tag %}}

Contexte du défi
----------------

L’utilisateur remarque que `action du driver malveillant` sur son PC. Vous investiguez.


Infrastructure à mettre en place
--------------------------------

L’infrastructure est simple : il faut un PC capable de charger un driver UEFI malveillant (le vôtre)

Pas-à-pas
---------

Vous devez créer un driver UEFI malveillant.   
Le driver fait des actions malveillantes et impacte le système du joueur.   
Le joueur doit reconnaître les impacts, comprendre les actions, reverse le driver.  
En reversant le driver il explique comment les actions ont eu lieu.

Risques
-------

La mise en place de l’exercice peut être compliquée.   
Il faut développer un driver UEFI ou modifier un bootkit.  
Exercice bas-niveau, il faut se documenter pour comprendre le fonctionnement de l’environnement UEFI.

Traces à enregistrer
--------------------

Fichiers à fournir :
- un dump de RAM de la machine cible
- des logs pertinents de l’attaque

Questions à poser :
- Questions sur le code de votre driver
- Questions sur les impacts du driver malveillant 


Liens
-----

* [Ressources sur vulnérabilités récentes](https://binarly.io/posts)
* [Ressources sur rootkit et bootkit](https://binarly.io/posts/The_list_of_highest-rated_books_for_Malware_Analysts_features_Rootkits_and_Bootkits)
* [Ressources UEFI et attaques sur UEFI](https://github.com/rmusser01/Infosec_Reference/blob/master/Draft/bios_uefi.md)