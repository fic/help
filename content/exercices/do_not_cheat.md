---
date: 2024-04-24T13:34:21+01:00
title: Tricher, c'est mal  
---

Les composants noyaux s'exécutant sur un système d'exploitation permettent une liberté d'action quasiment totale sur l'ensemble du système.

À partir d’un composant noyau malveillant, un attaquant compromet une machine critique au sein d’une entreprise ou d’un particulier.

Le logiciel malveillant doit mimer le comportement d'un cheat noyau.

Ce Petit-Exercice est aussi l'occasion de voir comment fonctionne un cheat pour votre jeu préféré.
/!\ Merci de ne pas l'utiliser hors de votre PFE.

Ce Petit-Exercice peut être utilisé par plusieurs groupes facilement en changeant l'OS du composant noyau.

{{% tag %}}Bas niveau - Noyau{{% /tag %}}

{{% tag %}}Driver{{% /tag %}}

{{% tag %}}Très difficile{{% /tag %}}

Contexte du défi
----------------

L’utilisateur remarque que son PC ne fonctionne pas très bien depuis qu'il utilise le dernier cheat pour son jeu préféré.


Infrastructure à mettre en place
--------------------------------

Il faut un PC capable de charger un driver noyau

Pas-à-pas
---------

Vous devez créer un driver noyau linux/windows/macos malveillant.   
Le driver fait des actions malveillantes et impacte le système du joueur.   
Le joueur doit reconnaître les impacts, comprendre les actions, reverse le driver.  
En reversant le driver il explique comment les actions ont eu lieu.

Risques
-------

La mise en place de l’exercice peut être compliquée.   
Il faut développer ou modifier un driver Windows/Linux ou MacOS.  
Exercice bas-niveau, il faut se documenter pour comprendre le fonctionnement de l’environnement noyau de votre OS favori.

Traces à enregistrer
--------------------

Fichiers à fournir :
- un dump de RAM de la machine cible
- des logs pertinents de l’attaque

Questions à poser :
- Questions sur le code de votre driver
- Questions sur les impacts du driver malveillant 


Liens
-----

* [Ressources sur le développement noyau Windows](https://learn.microsoft.com/en-us/windows-hardware/drivers/gettingstarted/)
* [Ressources sur le développement noyau MacOS](https://developer.apple.com/documentation/kernel)
* [Ressources sur le développement noyau Linux](https://lwn.net/Kernel/LDD3/)
* [How to become an evil cheater](https://www.unknowncheats.me/forum/anti-cheat-bypass/271733-driver-aka-kernel-mode.html)