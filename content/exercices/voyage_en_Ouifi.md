---
date: 2024-04-04
title: 
---

{{% tag %}}facile{{% /tag %}}

{{% tag %}}Osint{{% /tag %}}


Contexte du défi
----------------

L'idée est de jouer le role d'un enquêteur voulant retrouver le parcours d'un criminel arrêté. Après investigation de son téléphone on a pu récupérer les logs de tous les wifi qu'il a pu récupérer (trouver une raison / application / justification que ces logs sont stocké, un peu comme si il avait un "iwlist scan" en permanence, on peut justifier qu'il s'agit d'un pc si sur téléphone ce n'est pas possible).

Le parcours du criminel pourrait être analysé en comparant les ssid découvert à une carte publique des ssid (comme sur https://wigle.net/).

Libre court à votre imagination pour trouver des questions à poser / lieux visité / etc.. mais je suppose qu'il y moyen de faire un truc marrant.


Infrastructure à mettre en place
--------------------------------

- Soit vous générez vos données
- soit un setup (téléphone/ pc) pouvant logs les wifi, et vous faites vous même le parcours dans paris par exemple. (plus fun?)

Sachant que vous pouvez contribuer aussi à wiggle en exportant sur le site des resultats de découvertes de ssids (https://wigle.net/uploads)

Pas-à-pas
---------



Risques
-------


Traces à enregistrer
--------------------

Fichiers à fournir :

- les logs wifi

Questions à poser :

- libre court à votre imagination, c'est une sorte d'osint du coup il y a pas mal de possiblités


Liens
-----

- https://wigle.net/
- https://openwifimap.net/