---
date: 2023-04-08
title: Pêche au coquillage
---

Malgré de nombreuses tentative de se protéger contre cela, les document office servent toujours dans de nombreuses attaques, que ça soit pour de la malveillance comme pour de l'audit de sécurité (sensibilisation ou red team).

Le but de cette exercice est à partir d'une copie de boites mail de plusieurs personnel de l'entreprise de retrouver le document malveillant, de l'analyser pour en retirer des IOC permettant une étude plus approfondie.

{{% tag %}}Office{{% /tag %}}

{{% tag %}}Phishing{{% /tag %}}

{{% tag %}}Windows{{% /tag %}}

{{% tag %}}Moyen{{% /tag %}}


Contexte du défi
----------------

Un ransomware s'est déclaré dans l'entreprise ! Vous faite partie de l'équipe chargée de gérer cette réponse à incident. Votre mission est d'essayer de comprendre par où l'attaquant à pu entrer, pour cela vous avez comme tâche préliminaire d'étudier les échanges mails des personnes du service compromis.

Il y a t il des éléments d'intérêt ?


Infrastructure à mettre en place
--------------------------------

- Serveur de mail
- plusieurs poste pouvant communiquer avec le serveur de mail
- un serveur vers lequel votre document office malveillant doit pouvoir communiquer (un C&C quelconque fera l'affaire, il ne sera pas donné au joueur ici)


Pas-à-pas
---------

- analyse des boites mails
- récupération du document office (il faudrait que ça ne soit pas le seul)
- analyse du document
- récupération de la macro & analyse
- extraction de son comportement & IOC


Risques
-------

- Les ip doivent être réaliste, avec des noms de domaines réaliste. Il faudra bien setup les serveurs dns de votre infra pour ça

- la macro ne doit pas être un truc trop générique (pas un truc basique généré par métasploit, idéalement généré par vous meme avec un peu d'obfuscation ?)

- les mails doivent être suffisamenet nombreux et pertinent (mix de vrai message et pub peuvent faire l'affaire). Avec une temporalité pertinente

Traces à enregistrer
--------------------

- extractions des boites mails

- génération d'un document pertinent avec une vraie macro faisait une action malveillante (droppper ? execution de code ?  au choix)


Liens
-----

