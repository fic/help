---
date: 2023-03-24T13:34:21+01:00
title: Allô ? Allô… Comment ça ma pile audio ne marche pas ?
---


C’est le moment de travailler sur un programme malveillant impactant le système linux mobile de votre choix.

L’objectif est de comprendre comment fonctionne les composants du téléphone  (messages, voix, …) cible et de détourner ses fonctionnalités.

Pour cet exercice, le téléphone tourne sous un autre système qu’Android ou iOS, ce qui rend l’exercice plus original, plus spécifique, plus dur.

Le but est que le joueur apprenne des spécificités d’un système linux pour téléphone portable.

{{% tag %}}Téléphone{{% /tag %}}

Contexte du défi
----------------

Votre superbe téléphone sous Linux est plus lent qu’à l’accoutumée. D’ailleurs, la pile audio ne fonctionnent pas.  
Voilà maintenant qu’il se met à chauffer sans que vous ne fassiez rien… Vous investiguez.

Infrastructure à mettre en place
--------------------------------

Un modèle de téléphone qui n’est pas sous Android ni iOS.

Un PinePhone par exemple ou un téléphone avec un système linux, [ubuntu touch](https://ubports.com/nl/supported-products) par exemple


Pas-à-pas
---------

Vous êtes libres de développer, de compromettre, les composants qui vous semblent intéressants pour mettre en exergue les spécificités d’un téléphone mobile sous Linux.

Risques
-------

Matériel (téléphone compatible avec Linux)

Moins de documentation car sujet potentiellement plus spécifique et moins populaire.

Traces à enregistrer
--------------------
Fichiers à fournir :
- Dump de ram de l’appareil
- Paquet ou application malveillante

Questions à poser :
- Insister sur les spécificités de l’appareil

Liens
-----

[Téléphones recommandés pour Ubuntu Touch](https://ubports.com/nl/supported-products)

[PinePhone](https://www.pine64.org/pinephone/)

