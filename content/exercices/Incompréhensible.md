---
date: 2023-04-08
title: Incompréhensible 
---

Analyse d'un malware utilisant utilisant une sorte de packinng avec un jeu d'instruction personnalisé.


{{% tag %}}reverse{{% /tag %}}

{{% tag %}}difficile{{% /tag %}}

{{% tag %}}TAG{{% /tag %}}


Contexte du défi
----------------

Libre


Infrastructure à mettre en place
--------------------------------

Un ordinateur infecté par le malware

Pas-à-pas
---------

Faire un programme implémentant un jeu d’instructions ressemblant à un assembleur (ou pseudo language ou ce que vous voulez)

Faire un système permettant d'interpréter ces instruction en "réelles instructions"

Dans un second temps votre malware devra soit récupérer du réseau soit, avec packé en lui, la liste de votre suite d'insutctions malveillante à executer pour effectuer ses actions.


Risques
-------

Il faut essayer de contrer au maximum l'analyse dynamique

Il faudrait y avoir suffisamment d’instruction diverses et variées dans votre système d’interprétation.

Ne pas utiliser directement d'outil déjà faits. Vous pouvez en modifier si le résultat est suffisamment différent.


Traces à enregistrer
--------------------

- Dump de RAM contenant le malware


Liens
-----

[Le packing](https://kindredsec.wordpress.com/2020/01/07/the-basics-of-packed-malware-manually-unpacking-upx-executables/)

[Reverse VMProtect binary](https://resources.infosecinstitute.com/topic/reverse-engineering-virtual-machine-protected-binaries/)