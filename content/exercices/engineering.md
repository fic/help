---
date: 2024-04-24T13:34:21+01:00
title: Engineering 
---

L'objectif de cet exercice est d'implémenter un engine SSL qui permettrait de réduire la sécurité cryptographique de celui qui l'utilise.

Le joueur devra comprendre ce qui a été rendu vulnérable et déchiffrer lui-même le contenu chiffré avec l'aide de l'engine.

{{% tag %}}Cryptographie{{% /tag %}}

{{% tag %}}OpenSSL Engine{{% /tag %}}

{{% tag %}}Facile / Moyen{{% /tag %}}

Contexte du défi
----------------

Une entreprise remarque que des informations internes se retrouvent divulguées sur Internet. Pourtant, ces informations sont chiffrées de bout en bout...


Infrastructure à mettre en place
--------------------------------

2 PCs et 1 routeur

Pas-à-pas
---------

Vous devez créer ou modifier un engine OpenSSL.   
L'engine doit être utilisé pour réduire la sécurité des secrets.   
Le joueur doit reconnaître les impacts, comprendre les actions de l'engine.  

Risques
-------

OpenSSL peut être assez lourd à prendre en main.

Traces à enregistrer
--------------------

Fichiers à fournir :
- un dump de RAM de la machine cible
- des logs pertinents de l’attaque

Questions à poser :
- Questions sur le code de votre engine
- Questions sur les impacts de l'engine
- Le contenu des fichiers chiffrés 


Liens
-----

* [openSSL](https://www.openssl.org)
* [openSSL engines](https://www.openssl.org/docs/man1.0.2/man3/engine.html)
* [openSSL SSTIC paper](https://www.sstic.org/media/SSTIC2023/SSTIC-actes/exploring_openssl_engines_to_smash_cryptography/SSTIC2023-Article-exploring_openssl_engines_to_smash_cryptography-goudarzi_valadon.pdf)
