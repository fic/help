---
date: 2024-04-24T13:34:21+01:00
title: Hello from the other side  
---

L'objectif de l'exercice est de faire communiquer deux appareils qui échangent des informations de manière discrète.

Le but est d'extraire des données d'un ordinateur n'ayant pas internet à un autre ordinateur qui jouera le rôle de l'extracteur. 

Les deux appareils dialoguent via des sons de fréquences très hautes ou très basses de sorte qu'un être humain ne puisse pas entendre la conversation.

Vous pouvez adapter le moyen de communication.

{{% tag %}}Beep boop{{% /tag %}}

{{% tag %}}Difficile{{% /tag %}}

Contexte du défi
----------------

Vous remarquez que certains de vos appareils réagissent bizarrement ces derniers temps. Les capteurs de sons détectent constamment des interférences. Le joueur doit investiguer.

Vous pouvez adopter le contexte en fonction de votre élément déclencheur ou techno utilisée.


Infrastructure à mettre en place
--------------------------------

2 PCs

Pas-à-pas
---------

Vous devez mettre en place un protocole d'échange de données.
Les deux machines doivent communiquer entre elles sans connexion internet ou locale physique.

Risques
-------


Traces à enregistrer
--------------------

Fichiers à fournir :
- des enregistrements qui permettent d'élucider l'affaire
- quand le joueur a compris, le contenu du logiciel qui permet l'échange

Questions à poser :
- La méthode utilisée
- Comment fonctionne le protocole
- Le fonctionnement du logiciel

Liens
-----
