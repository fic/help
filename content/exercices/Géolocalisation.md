---
date: 2023-04-08
title: Géolocalisation
---

Utiliser un système d'antenne bluetooth pour arriver à géolocaliser le chemin d'un individu malveillant dans un supermarché.

{{% tag %}}bluetooth{{% /tag %}}

{{% tag %}}moyen?{{% /tag %}}


Contexte du défi
----------------

Votre super nouveau magasin hightech est équipé d'un système de capteurs bluetooth dispersé dans le magasin permettant le tracking en temps réel de tout le téléphone ayant ce dernier activé (soit quasiement tout les clients).

Cependant un individu semble s'être échappé en courant sans que nous ayons pu l'attraper ... malheureusement les caméras étaient en maintenance à ce moment donc nous ne savons pas ce qu'il a pu dérober.

Sauriez vous retracer son chemin et voir sur quel rayon s'est-il attardé ?


Infrastructure à mettre en place
--------------------------------

- Avoir un nombre suffisant de capteur bluetooth permettant d'essayer de faire une triangulation d'une source émettrice et les dispersers dans une zone relativement étendu (comme le lab)

- faire une carte de la zone qui sera notre supermarché avec des rayons etc


Pas-à-pas
---------

Libre.


Risques
-------

- Avoir suffisament de capteur (au moins 6/7 serait le minimum je pense mais bon à voir)


Traces à enregistrer
--------------------

- Enregistrement bluetooth
- Carte du magasin


Liens
-----

