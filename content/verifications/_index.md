---
date: 2022-06-12T13:46:52+02:00
title: Vérifications à faire
weight: 20
---

Avant de passer votre soutenance, lorsque vous avez terminé une étape, prennez le temps de vérifier les points suivants. C'est ces points qui seront vérifiés par les assistants pour valider votre étape :

{{% children %}}
