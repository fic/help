---
date: 2022-06-12T14:15:15+02:00
title: Étape
weight: 15
---

## Le contexte

Il faut veiller à ce que l'étape soit largement décrite dans le moindre détail.

Minimum 3 paragraphes pour poser le contexte (ne pas hésiter à faire des redits par rapport à `overview.md` ou aux `statement.md` précédents, car les participants peuvent passer d'un scénario à l'autre) et expliquer ce qui est attendu.

Ici, le storytelling n'est pas important (c'est toujours mieux s'il y a), les informations données sont purement techniques, à destination des participants. À la lecteure du `statement.md` ils doivent savoir exactement quelles informations rechercher.


# Les flags

## Les intitulés des flags

Vérifiez l'orthographe !

Vérifiez la présence **OBLIGATOIRE** d'un placeholder qui montre le format attendu du flag :

- `x.x.x.x` ou `127.0.0.1` ou `0.0.0.0`, ... pour une IP,
- `0123456789abcdef0123456789abcdef` pour un hash,
- `john.doe` pour un login,
- `secret_password` si c'est un mot de passe,
- ...

Assurez-vous que le label soit une description du champ « Mot de passe de l'administrateur » ou « Hash du fichier compromis », et PAS ~~« Quel est le hash du fichier compromis »~~ <-- c'est pour ça que `repochecker` vous embête pour ne pas mettre de `?`, on ne formule pas une question, on décrit le contenu attendu dans le champ.
