---
date: 2022-06-12T13:48:35+02:00
title: Scénario général
weight: 10
---

## Sur la page d'accueil


- 1 image sympa ;
- 1 titre sympa : pas de langage familier, ça peut être un jeu de mot, le nom de l'entreprise, ... soyez créatifs ;
- 1 à 2 phrases d'accroche bien conçues : c'est-à-dire qui présentent bien, sans s'étendre, mais faut vraiment que ce soit court, environ 6 lignes, 9-10 c'est trop.


## Sur la page du scénario

### Contexte du scénario `overview.md`

Il faut 3 à 4 paragraphes (**pas moins !**) qui présentent le contexte, et le présentent bien, avec du beau storytelling et sans faute d'orthographe : quel est le secteur de votre entreprise, quelles sont ses activités habituelles, comment s'est passé la découverte de l'incident de sécurité, quelles premières mesures ont été prises (s'il y en a eu), quels sont nos interlocuteurs sur place, où se trouve-t-on, ...

Il faut aussi **au moins 1** schéma (**obligatoire**) qui ne dévoile aucune information pour la suite, mais qui présente convenablement le contexte : cela peut être un schéma d'architecture du réseau (dans lequel on voit les différents équipements ainsi que les séparations, ...)

Pour le texte, il faut essayer autant que possible que ce soit compréhensible par un décideur, d'où le storytelling qui compte davantage que les détails techniques précis. Attention de ne pas donner d'informations qui dévoilent les flags.

Le schéma attendu correspond à un schéma que l'équipe IT donnerait aux auditeurs lorsqu'ils arrivent sur place : il peut y avoir des éléments manquant (on ne sait jamais si le schéma est bien à jour !), profitez-en pour omettre sciemment les détails que vous souhaitez cacher s'ils donnent trop d'indication sur le reste des étapes.


### Présentation des étapes `X-Titre/overview.md`

1 à 2 phrases par étape qui décrivent bien l'étape : toujours sans donner d'informations capitales, mais il faut que cela résume bien ce qui sera fait dedans.

Au-delà de ces deux phrases (c'est la première ligne du fichier `overview.md` qui est extraite), on peut détailler un peu plus ce qui est attendu, pour que ce soit compréhensible par un décideur.

La suite de ce texte est accessible aux participants lorsqu'ils cliquent sur une étape pour laquelle il n'ont pas encore l'accès. On le retrouve aussi sur le tableau de bord visible du public. C'est pour cela qu'il faut faire un effort pour être le plus pédagogue possible. Le texte est affiché seul, sans le reste du contexte, donc n'hésitez pas à réexpliquer succinctement le contexte.

Pas plus de 2 à 3 paragraphes, la première ligne doit mettre dans l'action et donner envie de lire la suite.
