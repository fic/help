---
date: 2023-11-26T20:01:12+02:00
title: Le tableau de bord
weight: 60
---

Le tableau de bord se contrôle via la page "Public" de l'interface d'administration.

9 écrans peuvent être utilisés en parallèle, on y accès via les fichiers `publicX.html`, où `X` est un nombre entre 1 et 9.


## Restreindre l'accès au tableau de bord

Sur l'IP de `deimos`, le port 8083 est ouvert afin de rendre le tableau de bord accessible à des personnes sans connexion SSH (par exemple pour que les organisateurs puissent le diffuser).

Afin de limiter son accès, il est possible de restreindre les IP qui peuvent le contacter.
Pour cela, il faut créer un fichier à cet emplacement sur `deimos` : `/var/lib/fic/dashboard/restricted-ips.json`.

Le contenu du fichier est un tableau JSON des préfixes d'IP autorisées :

```
[
  "192.168.0.1",
  "127.0.0.1"
]
```

Ou encore :

```
[
  "192.168.0.",
  "127.0.0.1"
]
```

Ce dernier exemple autorise toutes les adresses commençant par 192.168.0. à se connecter.
