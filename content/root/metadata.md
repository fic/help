---
date: 2025-01-24T11:01:00+01:00
title: Métadonnées fickit
weight: 51
---

Les métadonnées permettent d'adapter la configuration des machines sans avoir à recréer de nouvelles images LinuxKit. Le principe utilisé repose sur [les métadonnées que l'on donner à n'importe quel fournisseur de cloud](https://github.com/linuxkit/linuxkit/blob/master/docs/metadata.md).

On y retrouve la configuration du réseau, la clef de chiffrement des disques, certains paramètres fixes du challenge tels que le token pour remonter les scores, le domaine utilisé, les clefs SSH des roots, ...

Au démarrage, les machines vont rechercher un CD (virtuel dans notre cas) contenant un fichier `user-data` et en extraire les informations. L'image fickit est ensuite paramétrée pour utiliser ces informations lors du lancement des conteneurs.


## Éditer les métadonnées

Les différents paramètres se trouvent dans le script `configs/gen_metadata.sh` du dépôt [`server`](https://gitlab.cri.epita.fr/ing/majeures/srs/fic/server/), dans des variables d'environnement.

Une seule personne devrait se charger de la génération des métadonnées pour éviter que des parmètres soient perdus d'une édition à l'autre, puisqu'on ne commit pas les changements de variables surtout lorsqu'il s'agit de paramètres sensibles.


## Toute première génération des métadonnées

Vérifiez que le contenu des variables correspondent bien à ce qui est attendu pour le challenge.

Puis générer l'iso en appelant la commande : `./configs/gen_metadata.sh`

Lors de la première génération, plusieurs éléments seront générés à partir de l'aléatoire de votre système, ce qui peut prendre du temps : paramètre DH pour le serveur web, clef de chiffrement des disques, clefs SSH entre les machines, ...

Ceci va générer un fichier `fickit-metadata.iso` dans votre répertoire courant. Ce fichier est à placer auprès des images `fickit-...`.


## Regénération des métadonnées

Une fois les métadonnées déployées, on les met à jour de la même manière que l'on met à jour les images fickit.

{{% notice warning %}}
Lorsque le challenge a démarré, il est important de ne surtout pas changer les clefs de chiffrement des disques, sans quoi ils seraient rendus illisibles !
{{% /notice %}}

Pour mettre à jour certains paramètres, il faut récupérer l'ISO actuellement déployée (en la demandant au camarade qui l'a générée ou en allant la récupérer dans `/boot` des machines, il faut monter `/dev/sda1` ou `/dev/md2`).

On peut ensuite générer une nouvelle image qui contiendra les mêmes éléments aléatoires, en donnant comme premier argument l'ISO récupérée :

```
./configs/gen_metadata.sh fickit-metadata.iso
```

Dans tous les cas, il vaut mieux faire une sauvegarde de l'ancien fichier de métatonnés déployé.


## Mise à jour sur les serveurs

Pour appliquer les nouvelles métadonnées, il faut mettre à jour les images `fickit-...` comme pour [une mise à jour du système]({{% relref "files.md#mise-à-jour" %}}).


### Message d'avertissement de changement des clefs de chiffrement

Si lors de la mise à jour vous rencontrez le message suivant :

```
DM-CRYPT key changed in metadata, are you sure you want to erase it? (y/N)
```

Dites `N` !

{{% notice warning %}}
Un message d'avertissement s'affiche durant le mise à jour si les clefs de chiffrement du disque semblent avoir changé.

À ce stade le fichier `/boot/imgs/fickit-metadata.iso` aura été écrasé, mais vous trouverez sa sauvegarde sous `/boot/imgs/fickit-metadata.iso.bak`. Ce dernier fichier devrait contenir les clefs actuellement en cours d'utilisation (si c'est la première tentative).

{{% /notice %}}

La machine pourra redémarrer avec les bonnes clefs tant que vous ne validez pas le changement de clef de chiffrement (mais avec les anciens paramètres). Car les métadonnées utilisées sont écrites dans une partition (`/dev/sda2` ou `/dev/md3`). Les fichiers de la partition `/boot` ne sont pas utilisé durant le démarrage, seulement pour permettre la mise à jour.

Il est donc toujours possible de retrouver les clefs de chiffrement tant que cette partition n'aura pas été écrasée en passant outre l'avertissement. Par exemple avec un :

```
dd if=/dev/sda2 of=/tmp/fickit-metadata.iso
```
