---
date: 2023-10-22T10:04:12+02:00
title: L'image fickit-deploy
weight: 20
---

L'image `nemunaire/fickit-deploy` permet de démarrer en PXE les serveurs pour les installer initiallement (lorsque les disques sont vierges) et de mettre à jour les images.

## Arborescence attendue

L'image a besoin des derniers artifacts d'un build fickit réussi :

```
fickit-backend-squashfs.img
fickit-boot-initrd.img
fickit-boot-kernel
fickit-frontend-squashfs.img
fickit-prepare-initrd.img
fickit-update-initrd.img
```

Ainsi que des métadonnées du challenge.
Celles-ci peuvent être générées en [utilisant le script `configs/gen_metadata.sh`]({{% relref "metadata.md" %}}).


## Usage

1. Éteindre son pare-feu.
2. Lancer le conteneur :

```
docker run --network host --cap-add NET_ADMIN -v $(pwd):/srv/s nemunaire/fickit-deploy
```

### Variables

Le chemin `$(pwd)` doit correspondre au dossier où l'on trouve les artifacts et les métadonnées.

Il est possible de changer le comportement de l'image avec les variables suivantes :

- `PXE_IFACE` : nom de l'interface à utiliser (par défaut l'interface possédant la route par défaut) ;
- `DEPLOY_NETWORK` : préfixe de l'adresse IP à utiliser (par défaut `192.168.255`).


## Qu'est-ce que ça fait ?

`fickit-deploy` est une image permettant de lancer :

- un serveur DHCP sur 192.168.255.0/24
- un serveur TFTP sur 192.168.255.2
- un serveur HTTP sur 192.168.255.2

L'IP 192.168.255.2 est ajoutée automatiquement sur l'interface par défaut si elle n'est pas déjà configurée.
