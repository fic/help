---
date: 2022-12-10T13:42:27+01:00
title: Configuration iDRAC
weight: 1
---

### Changement du mot de passe
Accedez au BIOS via la touche F2.
Puis dans **iDRAC Settings > User Configuration > Change Password**, changez le mot de passe.

![idrac1](/img/idrac/idrac1.png)

![idrac2](/img/idrac/idrac2.png)

![idrac3](/img/idrac/idrac3.png)


### Vérification de l'IP

Pour obtenir l'IP du serveur rendez-vous dans **iDRAC Settings > Network** et la configuration réseau est affichée.
![idrac4](/img/idrac/idrac4.png)

![idrac5](/img/idrac/idrac5.png)

![idrac6](/img/idrac/idrac6.png)

Quittez le BIOS.

### Mettre à jour la licence

Nous utilisons une licence d'évaluation 14e génération iDRAC 9 Entreprise (30 jours).
Elle est nécéssaire pour utiliser la console virtuelle.
Téléchargez la licence à l'adresse suivante : https://www.dell.com/support/kbdoc/fr-fr/000176472/licences-d-evaluation-idrac-cmc-openmanage-enterprise-openmanage-integration-with-microsoft-windows-admin-center-openmanage-integration-with-servicenow-et-dpat#noteAgreeToDownload

![idrac7](/img/idrac/idrac7.png)

Importez ensuite la license dans **Configuration > Licences** depuis l'interface de l'iDRAC.

![idrac8](/img/idrac/idrac8.png)

![idrac9](/img/idrac/idrac9.png)

Pour que la licence soit prise en compte redémarrez le serveur.
