---
date: 2023-11-26T19:46:12+02:00
title: Connexion SSH et administration du challenge
weight: 55
---

## Se connecter en SSH aux machines

### `deimos`

`deimos` expose un serveur ssh sur les ports `admin` du switch.
Actuellement, l'IP du serveur est `192.168.3.92` (peu changer selon le plan d'adressage fourni par les organisateurs).

### `phobos`

Il n'est pas possible de se connecter à `phobos`.
Cette machine expose un serveur SSH uniquement à des fins de synchronisation des fichiers.
Le conteneur exécutant le serveur SSH ne permet pas de rebondir sur le reste du système, ce qui rend de toute manière la connexion assez inutile.

Pour administrer la machine, il faut donc utiliser l'iDRAC.


## Gestion des clefs SSH

Pour pouvoir se connecter au serveur, il est nécessaire d'avoir une clef SSH autorisée.

Les clefs SSH font parties des [métadonnées]({{% relref "fickit-deploy.md" %}}).


## Interface d'administration

Afin d'éviter que des personnes puissent accéder à l'interface d'administration, celle-ci n'est accessible que via la connexion SSH :

```
ssh -L 8081:127.0.0.1:8081 root@192.168.3.92
```

Cela ouvre un tunnel entre le port 8081 de votre système et le port 8081 du serveur.
Vous pourrez ensuite accéder à l'interface d'administration directement depuis votre machine à l'adresse : <http://localhost:8081/>.


## Transférer des fichiers hors des machines

Tous les fichiers nécessaire au challenge sont stockés dans le répertoire `/var/lib/fic`.
On y trouve à la fois les répertoires courants de la plateforme (`teams`, `submissions`, ...), mais aussi le répertoire de la base de données `mysql`, les sauvegardes, ...

Ce répertoire n'est pas partagé avec le conteneur `sshd` auquel on se connecte.
En dehors des fichiers bruts du challenge, il n'est donc pas possible d'utiliser `rsync` ou `scp` directement.

Seul le répertoire `/var/lib/fic/outofsync` est partagé.

Il convient donc de copier le fichier que l'on souhaite partager vers ce dossier, afin de le retrouver dans le conteneur `sshd` :

```
ssh root@192.168.3.92
nsenter -t 1 -a
cp /var/lib/fic/backups/db-20421225000000.sql.xz /var/lib/fic/outofsync
exit # On sort du système hôte pour revenir dans le conteneur sshd
exit # On quitte la connexion SSH

scp root@192.168.3.92:/var/lib/fic/outofsync/db-20421225000000.sql.xz .
```
