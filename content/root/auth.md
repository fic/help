---
date: 2022-12-10T13:41:27+01:00
title: Gestion de l'authentification
weight: 1
---

### Fonctionnement

L’authentification est une brique indépendante et peut se faire de plusieurs manières. On en utilise actuellement 2 :

- soit l’authentification avec le Forge (utilisée principalement sur la plateforme de développement)
- soit l’authentification avec DexIdP (utilisée pour les FIC Blanc et le FIC à Lille)

Lorsqu’un utilisateur va accéder aux challenges, il va passer par nginx qui va le rediriger vers VouchProxy.
Ce dernier va vérifier si l’utilisateur est bien authentifié, sinon, il va l’envoyer sur l'IdP pour qu’il s’authentifie.

Une variable est ainsi passé à nginx, contenant soit l'adresse email (cas de la forge), soit directement un identifiant d'équipe.

On peut utiliser d'autres méthodes comme par exemple :

- l'authentification HTTP classique,
- les certificats clients (dans ce cas, l'identifiant du certificat est la variable passée à nginx),
- ...

Lorsque ce n'est pas un identifiant d'équipe qui est récupéré, l'utilisateur fera face à un écran d'inscription, pour créer/rejoindre une équipe.
Une fois qu'il aura rejoint une équipe, sa variable passée à nginx sera ajoutée comme “Utilisateurs associés” à l'équipe.


### Comment créer les groupes ?

Dans la plateforme d'administration, allez dans "Equipe" et cliquez sur "+ Ajouter une équipe".

![auth1](/img/auth/auth6.png)

Renseignez le nom de l'équipe.

Pour la champ "external_id", il n'est pas nécéssaire d'en fournir un, celui-ci sert seulement lors de la connexion à une API externe : par exemple pour les scores s'il y a besoin d'ID spécifiques.

Vous pouvez directement ajouter les adresses des utilisateurs, ils seront directement associé a ce groupe et cela passera l'étape de choix de groupe lors de l'enrôlement d'un joueur.

Vous pouvez cliquer sur "+ Create team".

![auth1](/img/auth/auth7.png)



### Comment s'authentifier ?

Lorsque vous arrivez sur la page du challenge, cliquez sur le bouton "Inscription", cela aura pour effet de vous envoyer sur la page d'authentification choisis à la configuration du site.

![auth1](/img/auth/auth1.png)

Une fois authentifié, choisissez votre équipe (cf. tutoriel précédent).

![auth2](/img/auth/auth2.png)

Renseignez vos informations.

![auth3](/img/auth/auth3.png)

Une fois vos informations renseignées, vous devriez voir le nom de votre équipe, votre score, ainsi que votre classement par rapport aux autres équipes.

![auth4](/img/auth/auth4.png)

Pour administrer les groupes, aller dans "Equipe" (1) et choisissez en une.

Dans la partie "Membres" (2) vous trouverez les informations que le joueur a remplis lors de son enregistrement. Ces informations sont stockés dans la base de donnée dans la table "team_members".

Dans la partie "Utilisateurs associés" (3) vous trouverez les adresses qui ont été récupérés grâce à OIDC. (Cette association est stockée sous forme de lien symbolique dans le dossier `TEAMS`.)

![auth5](/img/auth/auth5.png)
