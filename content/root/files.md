---
date: 2023-11-24T10:46:12+02:00
title: Installation des serveurs
weight: 50
---

L'installation des serveurs se déroule en plusieurs parties :

- L'installation du système d'exploitation ;
- la synchronisation des fichiers ;
- la configuration de la plateforme.


## L'installation du système d'exploitation

Au sein du dépôt [`fic-server`](https://gitlab.cri.epita.fr/ing/majeures/srs/fic/server/), la CI construit les images tout-en-un qui devront être déployées sur les machines.

Des images bootables sont générées par [LinuxKit](https://github.com/linuxkit/linuxkit/), à partir des conteneurs Docker du projet.

Tous les fichiers de configuration sont embarqués dans les images. L'image étant en lecture seule, pour modifier un fichier de configuration il est nécessaire de recréer de nouvelles images.

Quelques éléments sont néanmoins variables, via l'apport de métadonnées. Chaque machine reçoit donc une image et un fichier de métadonnées.


### Identification des machines

Chaque machine a un rôle bien distinct :

- `phobos` : c'est la machine `frontend`, au contact des participants ;
- `deimos` : c'est la machine `backend`, protégée, contenant entre-autres la base de données et l'interface d'administration.

Il convient dans un premier temps de bien identifier l'image à installer sur chaque machine.


### Premier démarrage

La manière la plus rapide d'installer les machines est de les démarrer en PXE (au moyen de l'image Docker [`fickit-deploy`]({{% relref "fickit-deploy.md" %}})).

1. Récupérer les dernières images produites pour le système d'intégration continue.
1. (Demander aux roots le certificat et la clef privée pour le domaine live.fic.srs.epita.fr).
1. [Générer le fichier de métadonnées]({{% relref "metadata.md" %}}).

Il faut ensuite se brancher sur le switch, sur les ports `fic-mitm`.
Sur ces ports, on accède directement aux interfaces `eth0` des deux serveurs, la première interface utilisée par le BIOS pour démarrer en PXE.

Une fois le menu PXE récupéré, on choisit dans la liste l'opération que l'on souhaite voir réalisée.

{{% notice warning %}}
Le fichier de métadonnées contient entre-autres la clef de chiffrement des disques. Il faut garder en tête que cette clef ne doit en AUCUN CAS changer au cours d'une session. Si elle change, les disques seront définitivement illisibles.
{{% /notice %}}


### Mise à jour

Une fois les systèmes installer, ils peuvent se mettre à jour sans nécessiter de redémarrer en PXE.
Une entrée dans le menu de démarrage permet de lancer la procédure de mise à jour.
Celle-ci passe uniquement en HTTP.

Il convient de se brancher sur le switch, sur les ports `fic-mitm`.
L'image de mise à jour va faire une requête DHCP et récupérer les images depuis l'IP du routeur par défaut listé dans le paquet DHCP.

Toutes les images seront mises à jour, quelque soit le rôle de la machine.
Les métadonnées sont également mises à jour.

{{% notice warning %}}
Le fichier de métadonnées contient entre-autres la clef de chiffrement des disques. Cette clef ne doit changer SOUS AUCUN PRÉTEXTE dans la phase de mise à jour. Les disques seraient rendu illisibles, les données perdus et le système d'exploitation serait incapable de démarrer entièrement.

Si cela vous arrive, une sauvegarde du dernier fichier de métadonnées est effectué aux côtés du nouveau fichier.

Si la sauvegarde n'est pas la bonne, la seule option restante est de procéder à une réinstallation complète du système en PXE, afin que les disques soient formaté avec la nouvelle clef de chiffrement.
{{% /notice %}}


## Synchronisation des fichiers

Sur `deimos`, l'interface d'administration ne voudra vraisemblablement pas démarrer tant que le dossier contenant les fichiers bruts du challenge sera vide.
Sur `phobos`, plusieurs services dépendent de configurations apportées par l'interface d'administration (configuration Dex, PKI authentification, ...).

Une fois l'installation du système effectuée, il faut donc procéder à l'avitaillement :

1. Télécharger sur votre machine une copie intégrale du dépôt `challenges` :

    ```
	git clone --recursive --depth 1 --shallow-submodules --branch master git@gitlab.cri.epita.fr:ing/majeures/srs/fic/2042/challenges.git
	```

	Attention, ça prend facilement 50 à 100 GB !

	Vous devez avoir préalablement installé Git LFS, afin de récupérer les fichiers stockés sur le serveur LFS.

1. Récupérer les fichiers distants :

    Certains concepteurs ont hébergés leurs gros fichiers en dehors de git et de LFS.
	Cette étape est nécessaire afin de récupérer leurs fichiers et de donner aux serveurs une version du challenge entièrement hors-ligne.

	Vous pouvez utiliser pour cela l'utilitaire `get-remote-files` :

	```
	docker run --rm -v `pwd`/challenges:/mnt/fic nemunaire/fic-get-remote-files
	```

1. Transférer le répertoire `challenges` :

    ```
	rsync -av -e ssh challenges/* root@192.168.3.92:/mnt/fic/
	```

1. Redémarrer `deimos`.

Une fois redémarré, l'interface d'administration pourra démarrer et vous pourrez lancer la synchronisation intégrale.


## Configuration de la plateforme

1. Créer les équipes, attribuez-leur un mot de passe.
1. Générer le fichier de configuration pour `dex`, via le bouton "DexIdP"
1. Redémarrer `phobos`.

À l'issue de cette procédure, `dex` pourra démarrer avec une configuration adéquate.


## Débuger le système

Pour avoir un aperçu de l'état des services :

```
ctr -n services.linuxkit tasks ls
```

Sur chaque machine, dans le système hôte, les logs de tous les conteneurs sont enregistrés dans : `/var/log/`.

Sur `deimos`, lorsque l'on est connecté en ssh, on se trouve dans un conteneur qui n'est pas le système hôte.
Pour passer sur le système hôte, on utilise la commande :

```
nsenter -t 1 -a
```


## Connexion SSH et transfert de fichiers hors des machines

Voir [l'article dédié]({{% relref "ssh.md" %}})
