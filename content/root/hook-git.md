---
date: 2023-08-03T10:14:12+02:00
title: Hooks Git, CI/CD et synchronisations
weight: 20
---

Nous utilisons Gitlab CI afin de fluidifier et d'automatiser la mise à jour de la plateforme de challenges.

Afin d'éviter un trop grand nombre de conflit et/ou des impossibilités de synchroniser la plateforme, chaque groupe travaille dans un dépôt qui lui est propre (que ce soit un scénario ou un exercice).
Lorsque des modifications sont apportées à la branche principale, les modifications sont reportées dans le dépôt `challenges`, qui centralise les éléments du challenge.

On a donc d'une part le dépôt `challenges` : c'est celui qui est utilisé par la plateforme pour faire la synchronisation.
Il contient principalement des sous-modules vers les scénarios ou les exercices individuels de chaque groupe.

On peut découper le déploiement en 3 grandes étapes :

1. Lors d'un commit sur une branche principale d'un scénario ou d'un exercice individuel, une *merge request* est créée sur le dépôt `challenges`.
1. La *merge request* est testée pour s'assurer que son contenu ne va pas créer de problème de synchronisation.
1. La CI du dépôt `challenges` contacte la plateforme pour lui demander de récupérer les derniers changements et de lancer une synchronisation sur le scénario qui vient d'être changé.

## CI d'un dépôt de scénario/exercice individuel

Pour initier l'ensemble de la chaîne décrite ci-dessus, chaque groupe, avec l'aide des roots Fic, doit ajouter un fichier `.gitlab-ci.yml`.

Ce fichier importe le comportement décrit dans le dépôt `challenges` :

```yaml
include:
  - project: ing/majeures/srs/fic/2024/challenges
    file: /submodules-exercice-gitlab-ci.yml
```

pour un exercice individuel ; ou bien, pour un scénario :
```yaml
include:
  - project: ing/majeures/srs/fic/2024/challenges
    file: /submodules-scenario-gitlab-ci.yml
```

L'idée est de pouvoir adapter facilement le comportement de la CI en cas de bug ou de nouvelle fonctionnalité, sans devoir modifier chaque dépôt déjà configuré.

Le rôle de cette première chaîne est de :

- tester l'importabilité du dépôt sur la plateforme, le bon respect des consignes et des spécifications du projet. Le premier palier permet aux groupes d'avoir un retour rapide sur leurs modifications, sans avoir à attendre l'import f inal sur la plateforme (ou l'échec de cet import).
  Les tests incluent des tests qualité (orthographe, grammaire…) en plus sur la branche principale.
- Activation clef SSH déploiement
- Une fois ces tests passés, une *merge request* est créée dans le dépôt challenge afin d'y répercuter les dernières modifications fonctionnelles.

Pour ce faire, le dépôt exécute un *trigger* prédéfinit sur le dépôt challenge avec toutes les informations nécessaires pour créer la *merge request* :

- L'adresse du dépôt source ;
- l'identifiant du nouveau commit ;
- la branche concernée par le changement.


## CI du dépôt challenges

La création d'une *merge request* est initiée par chaque dépôt de scénario ou d'exercice individuel.

Selon les informations données, le trigger crée une nouvelle branche dans laquelle il applique le changement demandé.
S'il n'a encore jamais vu le dépôt en question, il ajoute le sub-module à un emplacement prédéfini.

La *merge request* est ensuite créée. Cela va lancer automatiquement une série de tests. La *merge request* sera fusionnée sitôt les tests réussis.
Il s'agit à ce stade de s'assurer que la fusion de ces modifications ne cause pas de problème d'imports sur la plateforme, au-delà du groupe fautif (par exemple si le groupe a omis des tests sur son propre dépôt).

Dès lors que la *merge request* est fusionnée sur la branche de destination, une dernière étape est lancée : la mise à jour de la plateforme.


## Mise à jour et synchronisation de la plateforme

Il faut distinguer 2 choses ici :

- La mise à jour du dépôt `challenges` : il s'agit de récupérer les dernières modifications en se plaçant sur le dernier commit de la branche suivie.
  Cela signifie donc qu'il faut également récupérer les modifications faites dans chaque sous-module, et dans chaque sous-module récupérer les fichiers stockés avec `git-lfs`.
- L'import sur la plateforme : il s'agit de lire le contenu du dépôt `challenges` tel qu'il est sur le disque de la machine et de répercuter les changements sur la plateforme.

La synchronisation peut se faire de différentes manière :

- synchronisation individuelle : pour mettre à jour uniquement un élément précis : *flags*, énoncé, liste de scénarios… Ce n'est jamais une synchronisation récursive ni destructrice.
- synchronisation intégrale : l'intégralité du dépôt est parcouru et tous les éléments sont récursivement mis à jour. Ce n'est pas une synchronisation destructive : les éléments qui n'existent plus sont préservés et les éléments dont une interaction a eu lieu sur la plateforme (tentative, validation…) ne seront pas mis à jour. Cela causera des erreurs non-critiques :
- synchronisation automatique : elle ne peut pas être déclenchée dans l'interface d'administration, c'est seulement une route d'API, ciblée par la chaîne de CI/CD. Il s'agit d'une synchronisation récursive, depuis une origine donnée (soit un scénario, soit un exercice) et destructrice : elle va effacer toutes les tentatives effectuées sur l'arborescence visée avant de tout reconstruire.

À la fin de chaque synchronisation, un rapport est généré et accessible à tous : <https://fic.srs.epita.fr/2024/admin/check_import.html>. Il contient les éventuels problèmes rencontrés.
