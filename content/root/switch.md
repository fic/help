---
date: 2023-07-27T08:56:27+02:00
title: Switch setup
weight: 10
---

## Reset the switch

1. reboot with powercoard
1. spam ctrl+b
1. option 7 (after boot)
1. validate with Y
1. option 0, aka reboot
1. after it finish to boot: `initalize`
1. validate with Y
1. wait reboot
1. press enter
1. auth with default cred (username: `admin` no password(emtpy))
1. set ip: `ipsetup ip-address 10.0.0.2 24 default-gateway 10.0.0.2`
1. set pasword with password command:
   ```
   <HP V1910 Switch>password
   Change password for user: admin
   Old password:
   Enter new password: ********************************
   Retype password: ********************************
   The password has been successfully changed.
   ```

   Note that old password is blank
1. quit
